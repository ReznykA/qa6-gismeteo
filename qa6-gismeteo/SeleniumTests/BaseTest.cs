﻿using System;
using Core;
using NUnit.Framework;
using PageMapping;

namespace SeleniumTests
{
    public class BaseTest : TestInstanceBase
    {
        protected Lazy<LoginPage> LoginPageLazy;
        protected Lazy<ResultsPage> ResultsPageLazy;
        protected Lazy<HomePage> HomePageLazy;
        protected Lazy<TopMenu> TopMenuLazy;
		protected Lazy<CenterBlockOfOldWeatherPage> CenterBlockOfOldWeatherPageLazy;
        protected Lazy<WeatherPageLinks> WeatherPageLinksLazy;
        protected Lazy<NewWeatherPage> NewWeatherPageLazy;

        protected override void InitPages()
        {
            LoginPageLazy = new Lazy<LoginPage>(() => new LoginPage(Browser));
            ResultsPageLazy = new Lazy<ResultsPage>(() => new ResultsPage(Browser));
            HomePageLazy = new Lazy<HomePage>(() => new HomePage(Browser).Load() as HomePage);
            TopMenuLazy = new Lazy<TopMenu>(() => new TopMenu(Browser).Load() as TopMenu);
            CenterBlockOfOldWeatherPageLazy = new Lazy<CenterBlockOfOldWeatherPage>(() => new CenterBlockOfOldWeatherPage(Browser).Load() as CenterBlockOfOldWeatherPage);
            WeatherPageLinksLazy = new Lazy<WeatherPageLinks>(() => new WeatherPageLinks(Browser).Load() as WeatherPageLinks);
            NewWeatherPageLazy = new Lazy<NewWeatherPage>(() => new NewWeatherPage(Browser).Load() as NewWeatherPage);

        }
        [OneTimeSetUp]
        public void SetUpBase()
        {
            OpenBrowser();
        }

        public LoginPage LoginPage => LoginPageLazy.Value.Load() as LoginPage;
        public ResultsPage ResultsPage => ResultsPageLazy.Value.Load() as ResultsPage;
        public HomePage HomePage => HomePageLazy.Value.Load() as HomePage;
        public TopMenu TopMenu => TopMenuLazy.Value.Load() as TopMenu;
		public CenterBlockOfOldWeatherPage CenterBlockOfOldWeatherPage => CenterBlockOfOldWeatherPageLazy.Value.Load() as CenterBlockOfOldWeatherPage;
        public WeatherPageLinks WeatherPageLinks => WeatherPageLinksLazy.Value.Load() as WeatherPageLinks;
        public NewWeatherPage NewWeatherPage => NewWeatherPageLazy.Value.Load() as NewWeatherPage;
    }
}
