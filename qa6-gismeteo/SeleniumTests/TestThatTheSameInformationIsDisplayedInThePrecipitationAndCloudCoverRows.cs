﻿using NUnit.Framework;
using PageMapping;

namespace SeleniumTests
{
    [TestFixture]
    class TestThatTheSameInformationIsDisplayedInThePrecipitationAndCloudCoverRows : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            FollowLink("http://gismeteo.ua");
            HomePage.CityOneLink.Click();
            WeatherPageLinks.OutdatedFormatLink.Click();
        }
        [Test]
        public void PrecipitationAndCloudCover()
        {
            string PrecipitationAlt = CenterBlockOfOldWeatherPage.ValuePrecipitation.GetAttribute("alt");
            string PrecipitationTitle = CenterBlockOfOldWeatherPage.ValuePrecipitation.GetAttribute("title");
            string CloudCoverAlt = CenterBlockOfOldWeatherPage.ValueOfCloudCover.GetAttribute("alt");
            string CloudCoverTitle = CenterBlockOfOldWeatherPage.ValueOfCloudCover.GetAttribute("title");
            Assert.AreEqual(PrecipitationAlt, CloudCoverAlt, "Precipitation should be the same as cloud cover");
            Assert.AreEqual(PrecipitationTitle, CloudCoverTitle, "Precipitation should be the same as cloud cover");
        }
    }
}
