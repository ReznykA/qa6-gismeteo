﻿using NUnit.Framework;
using SeleniumTests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FahrenheitSwitchingTest : BaseTest
{
    [TestFixture]
    public class Test1 : BaseTest
    {
        [OneTimeSetUp]
        public void SetUp()
        {
            OpenBrowser();
        }

        [Test]
        public void Case1()
        {
            FollowLink("gismeteo.ua");
        }
    }
}