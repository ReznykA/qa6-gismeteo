﻿using NUnit.Framework;
using PageMapping;

namespace SeleniumTests
{
	[TestFixture, Description ("Verify that all columns with weather parameters are present on the page")]
	class ColumnsIsPresent : BaseTest
	{	
		[SetUp]
        public void SetUp()
        {
            FollowLink("http://gismeteo.ua");
            HomePage.CityOneLink.Click();
            WeatherPageLinks.OutdatedFormatLink.Click();
        }
		
		[Test]
		public void VerifyCloudCover()

		{
			CenterBlockOfOldWeatherPage.CloudCover.InnerTextEqualsTo("Облачность");
		}

		[Test]
		public void VerifyRain()
		{
			CenterBlockOfOldWeatherPage.Rain.InnerTextEqualsTo("Осадки");
		}

		[Test]
		public void VerifyTemperature()
		{
			CenterBlockOfOldWeatherPage.Temperature.InnerTextEqualsTo("Температура");
		}

		[Test]
		public void VerifyPressure()
		{
			CenterBlockOfOldWeatherPage.Pressure.InnerTextEqualsTo("Давление");
		}

		[Test]
		public void VerifyHumidity()
		{
			CenterBlockOfOldWeatherPage.Humidity.InnerTextEqualsTo("Влажность");
		}

		[Test]
		public void VerifyWind()
		{
			CenterBlockOfOldWeatherPage.Wind.InnerTextEqualsTo("Ветер");
		}

		[Test]
		public void VerifyTemperatureFelt()
		{
			CenterBlockOfOldWeatherPage.TemperatureFelt.InnerTextEqualsTo("Ощущается");
		}
	}
}
