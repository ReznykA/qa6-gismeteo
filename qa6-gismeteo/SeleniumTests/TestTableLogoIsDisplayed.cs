﻿using NUnit.Framework;
using PageMapping;

namespace SeleniumTests
{
    [TestFixture]
    public class TestTableLogoIsDisplayed : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            FollowLink("http://gismeteo.ua");
            HomePage.CityOneLink.Click();
            WeatherPageLinks.OutdatedFormatLink.Click();
        }
        [Test]
        public void CheckTableLogoIsDisplayed()
        {
            WeatherPageLinks.FiveDaysLink.Click();
            Assert.IsTrue(WeatherPageLinks.TableLogo.IsDisplayed, "Logo in the Table for 5 days forecast isn`t displayed");
            WeatherPageLinks.SevenDaysLink.Click();
            Assert.IsTrue(WeatherPageLinks.TableLogo.IsDisplayed, "Logo in the Table for 7 days forecast isn`t displayed");
            WeatherPageLinks.ThreeDaysLink.Click();
            Assert.IsTrue(WeatherPageLinks.TableLogo.IsDisplayed, "Logo in the Table for 3 days forecast isn`t displayed");
        }
    }
}
