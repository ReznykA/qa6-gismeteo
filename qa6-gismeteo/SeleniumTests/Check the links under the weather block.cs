﻿using NUnit.Framework;
using PageMapping;

namespace SeleniumTests
{
    [TestFixture]
    public class Check_the_links_under_the_weather_block : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            FollowLink("http://gismeteo.ua");
            HomePage.CityOneLink.Click();
            WeatherPageLinks.OutdatedFormatLink.Click();
        }

        [Test]
        public void CheckTheWeatherForBusyLink()
        {
            WeatherPageLinks.WeatherForBusyLink.Click();
            bool WeatherForBusyTitleDisplayed = WeatherPageLinks.WeatherForBusyTitle.IsDisplayed;
            Assert.True(WeatherForBusyTitleDisplayed, "The weather for busy title should be displayed");
        }

        [Test]
        public void CheckNewFormatLink()
        {
            WeatherPageLinks.NewFormatLink.Click();
            bool NewFormatDisplayed = WeatherPageLinks.NewFormatElement.IsDisplayed;
            Assert.True(NewFormatDisplayed, "New format of the page should be displayed");
            Assert.AreEqual("Краткий прогноз погоды", WeatherPageLinks.NewFormatElement.InnerText);
        }
        [Test]
        public void CheckHourlyWeatherForecastLink()
        {
            WeatherPageLinks.HourlyWeatherLink.Click();
            bool HourlyWeatherElementDisplayed = WeatherPageLinks.HourlyWeatherElement.IsDisplayed;
            Assert.True(HourlyWeatherElementDisplayed, "Hourly Weather Forecast should be displayed");
            Assert.AreEqual("Почасовой прогноз погоды", WeatherPageLinks.HourlyWeatherElement.InnerText);
        }
        [Test]
        public void CheckTheWeatherForTwoWeeksLink()
        {
            WeatherPageLinks.WeatherForTwoWeeks.Click();
            bool WeatherForTwoWeeksDisplayed = WeatherPageLinks.TwoWeeksElement.IsDisplayed;
            Assert.True(WeatherForTwoWeeksDisplayed, "The Weather For Two Weeks should be displayed");
        }
        [Test]
        public void CheckGeomagneticLink()
        {
            WeatherPageLinks.Geomagnetic.Click();
            bool GeomagneticDisplayed = TopMenu.GeomagneticTitle.IsDisplayed;
            Assert.True(GeomagneticDisplayed, "Geomagnetic should be displayed");
            Assert.AreEqual("Геомагнитная обстановка", TopMenu.GeomagneticTitle.InnerText);
        }
        [Test]
        public void CheckTheWeatherForMonthLink()
        {
            WeatherPageLinks.WeatherForMonth.Click();
            bool WeatherForMonthDisplayd = WeatherPageLinks.WeatherForMonthTitle.IsDisplayed;
            Assert.True(WeatherForMonthDisplayd, "The Weather For Month should be displayed");
            StringAssert.Contains(" на месяц", WeatherPageLinks.WeatherForMonthTitle.InnerText);
        }
    }
}
