﻿using NUnit.Framework;
using PageMapping;
using System.Collections.Generic;
using SeleniumTests.Utils;

namespace SeleniumTests
{
    [TestFixture]
    class HumidityPressureMycity : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            FollowLink("http://gismeteo.ua");

        }

        [Test]
        public void HumidityPressureMyCity()
        {
            string SelectedCity = HomePage.CityOneLink.InnerText;
            HomePage.CityOneLink.Click();
            List<string> HumidityNewThreeDays = CollectionConverter.ToList(NewWeatherPage.HumidityCollection);
            List<string> PressureNewThreeDays = CollectionConverter.ToList(NewWeatherPage.PressureCollection);
            string HumidityCurrentNew = NewWeatherPage.HumidityCurrent.InnerText;
            string PressureCurrentNew = NewWeatherPage.PressureCurrent.InnerText;
            NewWeatherPage.NextDay.Click();
            List<string> HumidityNewThreeDays2 = CollectionConverter.ToList(NewWeatherPage.HumidityCollection2);
            List<string> PressureNewThreeDays2 = CollectionConverter.ToList(NewWeatherPage.PressureCollection2);
            NewWeatherPage.NextDay2.Click();
            List<string> HumidityNewThreeDays3 = CollectionConverter.ToList(NewWeatherPage.HumidityCollection3);
            List<string> PressureNewThreeDays3 = CollectionConverter.ToList(NewWeatherPage.PressureCollection3);
            NewWeatherPage.FiveDays.Click();
            List<string> HumidityNewFiveDays = CollectionConverter.ToList(NewWeatherPage.HumidityCollection);
            List<string> PressureNewFiveDays = CollectionConverter.ToList(NewWeatherPage.PressureCollection);
            NewWeatherPage.NextDay.Click();
            List<string> HumidityNewFiveDays2 = CollectionConverter.ToList(NewWeatherPage.HumidityCollection2);
            List<string> PressureNewFiveDays2 = CollectionConverter.ToList(NewWeatherPage.PressureCollection2);
            NewWeatherPage.NextDay2.Click();
            List<string> HumidityNewFiveDays3 = CollectionConverter.ToList(NewWeatherPage.HumidityCollection3);
            List<string> PressureNewFiveDays3 = CollectionConverter.ToList(NewWeatherPage.PressureCollection3);
            NewWeatherPage.SevenDays.Click();
            List<string> HumidityNewSevenDays = CollectionConverter.ToList(NewWeatherPage.HumidityCollection);
            List<string> PressureNewSevenDays = CollectionConverter.ToList(NewWeatherPage.PressureCollection);
            NewWeatherPage.NextDay.Click();
            List<string> HumidityNewSevenDays2 = CollectionConverter.ToList(NewWeatherPage.HumidityCollection2);
            List<string> PressureNewSevenDays2 = CollectionConverter.ToList(NewWeatherPage.PressureCollection2);
            NewWeatherPage.NextDay2.Click();
            List<string> HumidityNewSevenDays3 = CollectionConverter.ToList(NewWeatherPage.HumidityCollection3);
            List<string> PressureNewSevenDays3 = CollectionConverter.ToList(NewWeatherPage.PressureCollection3);
            WeatherPageLinks.OutdatedFormatLink.Click();
            string HumidityCurrentOld = CenterBlockOfOldWeatherPage.HumidityCollection[0].Text;
            string PressureCurrentOld = CenterBlockOfOldWeatherPage.PressureCollection[0].Text;
            List<string> HumidityOldThreeDays = CollectionConverter.ToList(CenterBlockOfOldWeatherPage.HumidityCollection);
            List<string> PressureOldThreeDays = CollectionConverter.ToList(CenterBlockOfOldWeatherPage.PressureCollection);
            WeatherPageLinks.FiveDaysLink.Click();
            List<string> HumidityOldFiveDays = CollectionConverter.ToList(CenterBlockOfOldWeatherPage.HumidityCollection);
            List<string> PressureOldFiveDays = CollectionConverter.ToList(CenterBlockOfOldWeatherPage.PressureCollection);
            WeatherPageLinks.SevenDaysLink.Click();
            List<string> HumidityOldSevenDays = CollectionConverter.ToList(CenterBlockOfOldWeatherPage.HumidityCollection);
            List<string> PressureOldSevenDays = CollectionConverter.ToList(CenterBlockOfOldWeatherPage.PressureCollection);
            for (int i = 0; i <= 3; i++)
            {
                Assert.AreEqual(HumidityNewThreeDays[i], HumidityOldThreeDays[i + 1]);
                Assert.AreEqual(HumidityNewThreeDays2[i], HumidityOldThreeDays[i + 5]);
                Assert.AreEqual(HumidityNewThreeDays3[i], HumidityOldThreeDays[i + 9]);
                Assert.AreEqual(HumidityNewFiveDays[i], HumidityOldFiveDays[i + 1]);
                Assert.AreEqual(HumidityNewFiveDays2[i], HumidityOldFiveDays[i + 5]);
                Assert.AreEqual(HumidityNewFiveDays3[i], HumidityOldFiveDays[i + 9]);
                Assert.AreEqual(HumidityNewSevenDays[i], HumidityOldSevenDays[i + 1]);
                Assert.AreEqual(HumidityNewSevenDays2[i], HumidityOldSevenDays[i + 5]);
                Assert.AreEqual(HumidityNewSevenDays3[i], HumidityOldSevenDays[i + 9]);
                Assert.AreEqual(PressureNewThreeDays[i], PressureOldThreeDays[i + 1]);
                Assert.AreEqual(PressureNewThreeDays2[i], PressureOldThreeDays[i + 5]);
                Assert.AreEqual(PressureNewThreeDays3[i], PressureOldThreeDays[i + 9]);
                Assert.AreEqual(PressureNewFiveDays[i], PressureOldFiveDays[i + 1]);
                Assert.AreEqual(PressureNewFiveDays2[i], PressureOldFiveDays[i + 5]);
                Assert.AreEqual(PressureNewFiveDays3[i], PressureOldFiveDays[i + 9]);
                Assert.AreEqual(PressureNewSevenDays[i], PressureOldSevenDays[i + 1]);
                Assert.AreEqual(PressureNewSevenDays2[i], PressureOldSevenDays[i + 5]);
                Assert.AreEqual(PressureNewSevenDays3[i], PressureOldSevenDays[i + 9]);

            }

            StringAssert.Contains(HumidityCurrentOld, HumidityCurrentNew);
            StringAssert.Contains(PressureCurrentOld, PressureCurrentNew);

            WeatherPageLinks.MyCity.Click();
            bool Message = WeatherPageLinks.MyCityMessage.WaitUntilIsDisplayed();
            Assert.True(Message, "The message should be displayed");
            WeatherPageLinks.MyCityMessage.InnerTextEqualsTo("Город Киев был успешно установлен как Ваш город");
            FollowLink("http://gismeteo.ua");
            string MyCity = HomePage.SelectedMyCity.InnerText;
            StringAssert.Contains(SelectedCity, MyCity, "My city should contain the selected city.");
        }
    }
}

