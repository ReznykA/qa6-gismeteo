﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumTests.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeleniumTests
{
    [TestFixture]
    public class TemperatureOldAndNewStyleTest : BaseTest
    {
        public TemperatureOldAndNewStyleTest()
        {
        }

        [Test]
        public void CheckOldAnDNewStyleTemperature()
        {
            FollowLink("https://www.gismeteo.ua/");
            HomePage.ForecastLink.Click();
            //List<int> TodayCelsiusList = CollectionConverter.ToIntList(NewWeatherPage.TodayCelsiusTemperature).ToList();
            TopMenu.SelectFahrenheit();
            //List<int> TodayFahrenheitList = CollectionConverter.ToIntList(NewWeatherPage.TodayFahrenheitTemperature).ToList();
            TopMenu.SelectCelsius();
            //ReadOnlyCollection TodayCelsiusList = NewWeatherPage.TodayCelsiusTemperature.ToList();

            var TodayTemp = NewWeatherPage.TodayCelsiusTemperature.ToList();
            List<int> a = new List<int>();
            foreach (IWebElement element in TodayTemp)
                a.Add(Convert.ToInt16(element.Text));
            


        }
        
    }
}
