﻿using NUnit.Framework;
using PageMapping;

namespace SeleniumTests
{
    [TestFixture]
    public class SelectedCity : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            FollowLink("http://gismeteo.ua");
            HomePage.CityOneLink.Click();
            WeatherPageLinks.OutdatedFormatLink.Click();
        }
        [Test]
        public void Selected_City()
        {
            string CurrentCity = WeatherPageLinks.SelectedCity.InnerText;
            FollowLink("http://gismeteo.ua");
            string SelectedCity = HomePage.CityOneLink.InnerText;
            StringAssert.Contains(SelectedCity, CurrentCity, "The current city should be equal to the selected city");
        }
    }
}
