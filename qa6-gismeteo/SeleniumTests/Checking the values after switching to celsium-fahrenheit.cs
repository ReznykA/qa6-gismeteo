﻿using NUnit.Framework;
using System;
using Core.ConvertMethods;

namespace SeleniumTests
{
    [TestFixture]
    public class CheckingTheValuesAfterSwitchingToCelsiumFahrenheit : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            FollowLink("http://gismeteo.ua");
            HomePage.CityOneLink.Click();
            WeatherPageLinks.OutdatedFormatLink.Click();
        }
        [Test]
        public void CheckTheTemperatureRowConvertCelsiumToFahrenheit()
        {
            TopMenu.SelectCelsius();
            String sCelsius1 = CenterBlockOfOldWeatherPage.ValueCelsius1.InnerText;
            int valueCelsius1 = Convert.ToInt32(sCelsius1);
            String sCelsius2 = CenterBlockOfOldWeatherPage.ValueCelsius2.InnerText;
            int valueCelsius2 = Convert.ToInt32(sCelsius2);
            String sCelsius8 = CenterBlockOfOldWeatherPage.ValueCelsius8.InnerText;
            int valueCelsius8 = Convert.ToInt32(sCelsius8);
            TopMenu.SelectFahrenheit();
            String sFahrenheit1 = CenterBlockOfOldWeatherPage.ValueFahrenheit1.InnerText;
            int valueFahrenheit1 = Convert.ToInt32(sFahrenheit1);
            String sFahrenheit2 = CenterBlockOfOldWeatherPage.ValueFahrenheit2.InnerText;
            int valueFahrenheit2 = Convert.ToInt32(sFahrenheit2);
            String sFahrenheit8 = CenterBlockOfOldWeatherPage.ValueFahrenheit8.InnerText;
            int valueFahrenheit8 = Convert.ToInt32(sFahrenheit8);

            Assert.AreEqual(valueFahrenheit1, ConvertGrad.CelsiusToFahrenheit(valueCelsius1), "The celsius to fahrenheit convertation error in the Temperature Row/column 1");
            Assert.AreEqual(valueFahrenheit2, ConvertGrad.CelsiusToFahrenheit(valueCelsius2), "The celsius to fahrenheit convertation error in the Temperature Row/column 2");
            Assert.AreEqual(valueFahrenheit8, ConvertGrad.CelsiusToFahrenheit(valueCelsius8), "The celsius to fahrenheit convertation error in the Temperature Row/column 8");
        }

        [Test]
        public void CheckTheFeltRowConvertCelsiumToFahrenheit()
        {
            TopMenu.SelectCelsius();
            String sCelsius6 = CenterBlockOfOldWeatherPage.FeltValueCelsius6.InnerText;
            int valueCelsius6 = Convert.ToInt32(sCelsius6);
            String sCelsius10 = CenterBlockOfOldWeatherPage.FeltValueCelsius10.InnerText;
            int valueCelsius10 = Convert.ToInt32(sCelsius10);
            String sCelsius13 = CenterBlockOfOldWeatherPage.FeltValueCelsius13.InnerText;
            int valueCelsius13 = Convert.ToInt32(sCelsius13);
            TopMenu.SelectFahrenheit();
            String sFahrenheit6 = CenterBlockOfOldWeatherPage.FeltValueFahrenheit6.InnerText;
            int valueFahrenheit6 = Convert.ToInt32(sFahrenheit6);
            String sFahrenheit10 = CenterBlockOfOldWeatherPage.FeltValueFahrenheit10.InnerText;
            int valueFahrenheit10 = Convert.ToInt32(sFahrenheit10);
            String sFahrenheit13 = CenterBlockOfOldWeatherPage.FeltValueFahrenheit13.InnerText;
            int valueFahrenheit13 = Convert.ToInt32(sFahrenheit13);

            Assert.AreEqual(valueFahrenheit6, ConvertGrad.CelsiusToFahrenheit(valueCelsius6), "The celsius to fahrenheit convertation error in FeltRow/column6");
            Assert.AreEqual(valueFahrenheit10, ConvertGrad.CelsiusToFahrenheit(valueCelsius10), "The celsius to fahrenheit convertation error in FeltRow/column10");
            Assert.AreEqual(valueFahrenheit13, ConvertGrad.CelsiusToFahrenheit(valueCelsius13), "The celsius to fahrenheit convertation error in FeltRow/column13");
        }

    }
}
