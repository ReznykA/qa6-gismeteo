﻿using NUnit.Framework;
using PageMapping;
using System;

namespace SeleniumTests
{
	[TestFixture, Description("Verify that three dates are correctly")]
	class ThreeCurrentDates : BaseTest
	{
		[SetUp]
        public void SetUp()
        {
            FollowLink("http://gismeteo.ua");
            HomePage.CityOneLink.Click();
            WeatherPageLinks.OutdatedFormatLink.Click();
        }
		
		[Test]
		public void VerifyTodayDate()

		{
			DateTime today = DateTime.Today;
			string DateExpected = today.ToString("dd-MMM-yyyy");

			DateTime dateActual1 = DateTime.Parse(CenterBlockOfOldWeatherPage.FirstDate.InnerText);
			string DateActual = dateActual1.ToString("dd-MMM-yyyy");

			Assert.AreEqual(DateActual, DateExpected, "Dates should be equal");
		}

		[Test]
		public void VerifyTomorrowDate()

		{
			DateTime tomorrow = DateTime.Today.AddDays(1);
			string DateExpected = tomorrow.ToString("dd-MMM-yyyy");

			DateTime dateActual1 = DateTime.Parse(CenterBlockOfOldWeatherPage.SecondDate.InnerText);
			string DateActual = dateActual1.ToString("dd-MMM-yyyy");

			Assert.AreEqual(DateActual, DateExpected, "Dates should be equal");
		}

		[Test]
		public void VerifyDayAfrerTomorrow()

		{
			DateTime dayAfterTomorrow = DateTime.Today.AddDays(2);
			string DateExpected = dayAfterTomorrow.ToString("dd-MMM-yyyy");

			DateTime dateActual1 = DateTime.Parse(CenterBlockOfOldWeatherPage.ThirdDate.InnerText);
			string DateActual = dateActual1.ToString("dd-MMM-yyyy");

			Assert.AreEqual(DateActual, DateExpected, "Dates should be equal");
		}
	}
}
