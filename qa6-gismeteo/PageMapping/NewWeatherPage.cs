﻿using Core;
using Core.Interfaces;
using Core.UIControls;
using Core.UIControls.Locators;
using Core.UIControls.SimpleControls;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Extentions;
using Core.Helpers;

namespace PageMapping
{
    public class NewWeatherPage : BasePage
    {
        public NewWeatherPage(IWebDriverExt<IWebDriver> driver) : base(driver)
        {
        }
        protected override bool EvaluateLoadedStatus()
        {
            return WaitHelper.WaitUntilElementIsDisplayed(MyCity, WaitTimeout.Small);
        }

        public ReadOnlyCollection<IWebElement> TodayCelsiusTemperature => SearchContextExtensions.GetWebElements(IoCResolver.WebDriver.WebDriver, new CssLocator("#tbwdaily1 .value.m_temp.c"));
        public ReadOnlyCollection<IWebElement> TodayFahrenheitTemperature => SearchContextExtensions.GetWebElements(IoCResolver.WebDriver.WebDriver, new CssLocator("#tbwdaily1 .value.m_temp.f"));
        public ReadOnlyCollection<IWebElement> HumidityCollection => SearchContextExtensions.GetWebElements(IoCResolver.WebDriver.WebDriver, new XpathLocator("//tbody[@id= 'tbwdaily1']//*[@class = 'wind']/parent::td/following-sibling::td[1]"));
        public ReadOnlyCollection<IWebElement> HumidityCollection2 => SearchContextExtensions.GetWebElements(IoCResolver.WebDriver.WebDriver, new XpathLocator("//tbody[@id= 'tbwdaily2']//*[@class = 'wind']/parent::td/following-sibling::td[1]"));
        public ReadOnlyCollection<IWebElement> HumidityCollection3 => SearchContextExtensions.GetWebElements(IoCResolver.WebDriver.WebDriver, new XpathLocator("//tbody[@id= 'tbwdaily3']//*[@class = 'wind']/parent::td/following-sibling::td[1]"));
        public ReadOnlyCollection<IWebElement> PressureCollection => SearchContextExtensions.GetWebElements(IoCResolver.WebDriver.WebDriver, new XpathLocator("//tbody[@id= 'tbwdaily1']//span[@class = 'value m_press torr']"));
        public ReadOnlyCollection<IWebElement> PressureCollection2 => SearchContextExtensions.GetWebElements(IoCResolver.WebDriver.WebDriver, new XpathLocator("//tbody[@id= 'tbwdaily2']//span[@class = 'value m_press torr']"));
        public ReadOnlyCollection<IWebElement> PressureCollection3 => SearchContextExtensions.GetWebElements(IoCResolver.WebDriver.WebDriver, new XpathLocator("//tbody[@id= 'tbwdaily3']//span[@class = 'value m_press torr']"));
        public CommonElement FiveDays => new CommonElement(new XpathLocator("//a[contains(@href, '3-5-days')]"), DefaultParent).Load() as CommonElement;
        public CommonElement SevenDays => new CommonElement(new XpathLocator("//a[contains(@href,'5-7-days')]"), DefaultParent).Load() as CommonElement;
        public CommonElement NextDay => new CommonElement(new CssLocator("div#tab_wdaily2"), DefaultParent).Load() as CommonElement;
        public CommonElement NextDay2 => new CommonElement(new CssLocator("div#tab_wdaily3"), DefaultParent).Load() as CommonElement;
        public CommonElement MyCity => new CommonElement(new CssLocator("a.jAjax"), DefaultParent).Load() as CommonElement;
        public CommonElement HumidityCurrent => new CommonElement(new CssLocator("div.wicon.hum"), DefaultParent).Load() as CommonElement;
        public CommonElement PressureCurrent => new CommonElement(new CssLocator("dd.value.m_press.torr"), DefaultParent).Load() as CommonElement;
    }
}
