﻿using Core;
using Core.Interfaces;
using Core.UIControls.Locators;
using Core.UIControls.SimpleControls;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Helpers;


namespace PageMapping
{
    public class HomePage : BasePage
    {
        public HomePage(IWebDriverExt<IWebDriver> driver) : base(driver)
        {
        }

        protected override bool EvaluateLoadedStatus()
        {
            return WaitHelper.WaitUntilElementIsDisplayed(ForecastLink, WaitTimeout.Small);
        }

        public CommonElement ForecastLink => new CommonElement(new XpathLocator("//a[@class='icon fcast']"), DefaultParent).Load() as CommonElement;
        public CommonElement CityOneLink => new CommonElement(new XpathLocator("//div[@id='cities-teaser']//a[contains(@href, '4944')]"), DefaultParent).Load() as CommonElement;
        public Label SelectedMyCity => new Label(new CssLocator("a.icon.mclink"), DefaultParent).Load() as Label;
        
    }
}
