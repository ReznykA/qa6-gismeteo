﻿using Core;
using Core.Helpers;
using Core.Interfaces;
using Core.UIControls;
using Core.UIControls.Locators;
using Core.UIControls.SimpleControls;
using OpenQA.Selenium;

namespace PageMapping
{
    public class WeatherPageLinks : BasePage
    {
        public WeatherPageLinks(IWebDriverExt<IWebDriver> driver) : base(driver)
        {
        }

        protected override bool EvaluateLoadedStatus()
        {
            return WaitHelper.WaitUntilElementIsDisplayed(MyCity, WaitTimeout.Small);
        }

        public Label WeatherOutdatedFormat => new Label(new CssLocator("div.h2.wtitle"), DefaultParent).Load() as Label;
        public Label SelectedCity => new Label(new CssLocator("h1.wtitle"), DefaultParent).Load() as Label;
        public CommonElement SelectedDaysElement => new CommonElement(new CssLocator("span.title.sel"), DefaultParent).Load() as CommonElement;
        public CommonElement ThreeDaysLink => new CommonElement(new XpathLocator("//a[contains(@href,'/legacy/')]"), DefaultParent).Load() as CommonElement;
        public CommonElement FiveDaysLink => new CommonElement(new XpathLocator("//a[contains(@href,'3-5')]"), DefaultParent).Load() as CommonElement;
        public CommonElement SevenDaysLink => new CommonElement(new XpathLocator("//a[contains(@href,'5-7')]"), DefaultParent).Load() as CommonElement;
        public CommonElement MyCity => new CommonElement(new CssLocator("a.jAjax"), DefaultParent).Load() as CommonElement;
        public CommonElement WeatherForBusyLink => new CommonElement(new XpathLocator("//a[contains(@href, 'busy')]"), DefaultParent).Load() as CommonElement;
        public CommonElement NewFormatLink => new CommonElement(new XpathLocator("//a[text() = 'Погода по-новому']"), DefaultParent).Load() as CommonElement;
        public CommonElement HourlyWeatherLink => new CommonElement(new XpathLocator("//a[contains(@href, 'hourly')]"), DefaultParent).Load() as CommonElement;
        public CommonElement WeatherForTwoWeeks => new CommonElement(new XpathLocator("//a[contains(@href, '14-days')]"), DefaultParent).Load() as CommonElement;
        public CommonElement Geomagnetic => new CommonElement(new XpathLocator("//a[contains(@href, '/gm')]"), DefaultParent).Load() as CommonElement;
        public CommonElement WeatherForMonth => new CommonElement(new XpathLocator("//a[contains(@href, 'month') and not(@class)]"), DefaultParent).Load() as CommonElement;
        public Label WeatherForBusyTitle => new Label(new XpathLocator("//div[@id='weather-busy']//div[@class = 'h2']"), DefaultParent).Load() as Label;
        public Label NewFormatElement => new Label(new XpathLocator("//li[@class = 'icon cur' and not(@a)]"), DefaultParent).Load() as Label;
        public Label OutdatedFormatLink => new Label(new XpathLocator("//li[@class = 'icon']/a[contains(@href, 'legacy')]"), DefaultParent).Load() as Label;
        public Label TwoWeeksElement => new Label(new XpathLocator("//span[@class = '2week title sel']"), DefaultParent).Load() as Label;
        public Label HourlyWeatherElement => new Label(new XpathLocator("//li[@class = 'icon cur' and not(@a)]"), DefaultParent).Load() as Label;
        public Label WeatherForMonthTitle => new Label(new CssLocator("h1.wtitle"), DefaultParent).Load() as Label;
        public Label MyCityMessage => new Label(new CssLocator("div.jhcontent p"), DefaultParent).Load() as Label;
        public Label TableLogo => new Label(new CssLocator(".wtlogo"), DefaultParent).Load() as Label;
    }
}
