﻿using Core;
using Core.Interfaces;
using Core.UIControls.Locators;
using Core.UIControls.SimpleControls;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Helpers;

namespace PageMapping
{
    public class TopMenu : BasePage
    {
        public TopMenu(IWebDriverExt<IWebDriver> driver) : base(driver)
        {
        }

        protected override bool EvaluateLoadedStatus()
        {
            return WaitHelper.WaitUntilElementIsDisplayed(SelectViewOptionsDropdown, WaitTimeout.Small);
        }

        private CommonElement SelectViewOptionsDropdown => new CommonElement(new XpathLocator("//li[@class='prev ifnoie']"), DefaultParent).Load() as CommonElement;
        private CommonElement SelectLanguageDropdown => new CommonElement(new XpathLocator("//li[@class='last ifnoie sprite']"), DefaultParent).Load() as CommonElement;
        private CommonElement CelsiusRadioButton => new CommonElement(new IdLocator("meas_chk_0"), DefaultParent).Load() as CommonElement;
        private CommonElement FahrenheitRadioButton => new CommonElement(new IdLocator("meas_chk_1"), DefaultParent).Load() as CommonElement;
        private CommonElement SelectUaLanguage => new CommonElement(new XpathLocator("//*[contains(text(),'Українською')]"), DefaultParent).Load() as CommonElement;
        private CommonElement SelectRuLanguage => new CommonElement(new XpathLocator("//*[contains(text(),'По-русски')]"), DefaultParent).Load() as CommonElement;
        public Label GeomagneticTitle => new Label(new CssLocator("div#geomagnetic div.h2"), DefaultParent).Load() as Label;

        public void SelectCelsius()
        {
            SelectViewOptionsDropdown.Click();
            CelsiusRadioButton.Click();
        }

        public void SelectFahrenheit()
        {
            SelectViewOptionsDropdown.Click();
            FahrenheitRadioButton.Click();
        }

        public void SelectUkraineLanguage()
        {
            SelectLanguageDropdown.Click();
            SelectUaLanguage.Click();
        }

        public void SelectRussianLanguage()
        {
            SelectLanguageDropdown.Click();
            SelectRuLanguage.Click();
        }
    }
}
