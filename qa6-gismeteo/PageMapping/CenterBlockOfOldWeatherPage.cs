﻿using Core;
using Core.Extentions;
using Core.Helpers;
using Core.Interfaces;
using Core.UIControls;
using Core.UIControls.Locators;
using Core.UIControls.SimpleControls;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace PageMapping
{
	public class CenterBlockOfOldWeatherPage : BasePage
	{
		public CenterBlockOfOldWeatherPage(IWebDriverExt<IWebDriver> driver) : base(driver)
		{
		}

		protected override bool EvaluateLoadedStatus()
		{
			return WaitHelper.WaitUntilElementIsDisplayed(WeatherBlock, WaitTimeout.Small);
		}

		public CommonElement WeatherBlock => new CommonElement(new CssLocator(".wdata-compact"), DefaultParent).Load() as CommonElement;
		public CommonElement CurrentDate => new CommonElement(new CssLocator(".current"), DefaultParent).Load() as CommonElement;
		public CommonElement FirstDate => new CommonElement (new XpathLocator("//th[contains(@class,'df')][1]"), DefaultParent).Load() as CommonElement;
		public CommonElement SecondDate => new CommonElement(new XpathLocator("//th[contains(@class,'df')][2]"), DefaultParent).Load() as CommonElement;
		public CommonElement ThirdDate => new CommonElement(new XpathLocator("//th[contains(@class,'df')][2]"), DefaultParent).Load() as CommonElement;
		public CommonElement Night => new CommonElement(new CssLocator("thead td.df"), DefaultParent).Load() as CommonElement;
		public CommonElement ValueOfCloudCover => new CommonElement(new XpathLocator("//tr[@class = 'cloudness']/td[1]/img"), DefaultParent).Load() as CommonElement;
		public CommonElement ValueOfTemperatureIn_C => new CommonElement(new CssLocator("span.value.m_temp.c"), DefaultParent).Load() as CommonElement;
		public CommonElement ValueOfTemperatureIn_F => new CommonElement(new CssLocator("span.value.m_temp.f"), DefaultParent).Load() as CommonElement;
		public CommonElement ValueOfPressureIn_Mm => new CommonElement(new CssLocator("span.value.m_press.torr"), DefaultParent).Load() as CommonElement;
		public CommonElement ValueOfPressureIn_Inch => new CommonElement(new CssLocator("span.value.m_press.inch"), DefaultParent).Load() as CommonElement;
		public CommonElement ValueOfHumidity => new CommonElement(new XpathLocator("//th[contains(text(),'%')]/following-sibling::td"), DefaultParent).Load() as CommonElement;
		public CommonElement ValueOfWindCommon => new CommonElement(new CssLocator("dl.wind"), DefaultParent).Load() as CommonElement;
		public CommonElement ValueOfWindIn_Meter => new CommonElement(new CssLocator("span.value.m_wind.ms"), DefaultParent).Load() as CommonElement;
		public CommonElement ValueOfWindIn_Mile => new CommonElement(new CssLocator("span.value.m_wind.mih"), DefaultParent);
        public CommonElement ValuePrecipitation => new CommonElement(new XpathLocator("//tr[@class = 'persp']/td[1]/img"), DefaultParent).Load() as CommonElement;

        public CommonElement CloudCover => new CommonElement(new CssLocator("tr.cloudness th"), DefaultParent).Load() as CommonElement;
		public CommonElement Rain => new CommonElement(new CssLocator("tr.persp th"), DefaultParent).Load() as CommonElement; 
		public CommonElement Temperature => new CommonElement(new XpathLocator("//th[contains(text(),'Температура')]"), DefaultParent).Load() as CommonElement;
		public CommonElement Pressure => new CommonElement(new XpathLocator("//span[contains(@class,'m_press')]/parent::th"), DefaultParent).Load() as CommonElement;
		public CommonElement Humidity => new CommonElement(new XpathLocator("//th[contains(text(),'%')]"), DefaultParent).Load() as CommonElement;
		public CommonElement Wind => new CommonElement(new XpathLocator("//span[contains(@class,'m_wind')]/parent::th"), DefaultParent).Load() as CommonElement;
		public CommonElement TemperatureFelt => new CommonElement(new XpathLocator("//*[contains(@class,'m_temp')]/parent::th[not (contains(text(),'Температура'))]"), DefaultParent).Load() as CommonElement;

        public CommonElement ValueCelsius1 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp c'])[1]"), DefaultParent).Load() as CommonElement;
        public CommonElement ValueCelsius2 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp c'])[2]"), DefaultParent).Load() as CommonElement;
        public CommonElement ValueCelsius8 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp c'])[8]"), DefaultParent).Load() as CommonElement;
        public CommonElement ValueFahrenheit1 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp f'])[1]"), DefaultParent).Load() as CommonElement;
        public CommonElement ValueFahrenheit2 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp f'])[2]"), DefaultParent).Load() as CommonElement;
        public CommonElement ValueFahrenheit8 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp f'])[8]"), DefaultParent).Load() as CommonElement;

        public CommonElement FeltValueCelsius6 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp c'])[19]"), DefaultParent).Load() as CommonElement;
        public CommonElement FeltValueCelsius10 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp c'])[23]"), DefaultParent).Load() as CommonElement;
        public CommonElement FeltValueCelsius13 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp c'])[26]"), DefaultParent).Load() as CommonElement;
        public CommonElement FeltValueFahrenheit6 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp f'])[19]"), DefaultParent).Load() as CommonElement;
        public CommonElement FeltValueFahrenheit10 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp f'])[23]"), DefaultParent).Load() as CommonElement;
        public CommonElement FeltValueFahrenheit13 => new CommonElement(new XpathLocator("(//span[@class = 'value m_temp f'])[26]"), DefaultParent).Load() as CommonElement;

        public ReadOnlyCollection<IWebElement> HumidityCollection => SearchContextExtensions.GetWebElements(IoCResolver.WebDriver.WebDriver, new XpathLocator("//th[contains(text(),'%')]/following-sibling::td"));
        public ReadOnlyCollection<IWebElement> PressureCollection => SearchContextExtensions.GetWebElements(IoCResolver.WebDriver.WebDriver, new CssLocator("span.value.m_press.torr"));
    }
}
