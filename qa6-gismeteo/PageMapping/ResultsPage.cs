﻿using Core;
using Core.Helpers;
using Core.Interfaces;
using Core.UIControls;
using Core.UIControls.Locators;
using Core.UIControls.SimpleControls;
using OpenQA.Selenium;

namespace PageMapping
{
	public class ResultsPage : BasePage
	{
		public ResultsPage(IWebDriverExt<IWebDriver> driver) : base(driver)
		{
		}

		public Label FirstAirport => new Label(new XpathLocator("//div[@class='districts wrap'][1]//span"), DefaultParent);

		//Example
		public Label SlowlyLoadedLabel => new Label(new IdLocator("textid"), DefaultParent).Load() as Label;

	}
}