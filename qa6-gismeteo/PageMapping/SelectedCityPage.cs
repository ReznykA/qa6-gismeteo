﻿using Core;
using Core.Interfaces;
using Core.UIControls.Locators;
using Core.UIControls.SimpleControls;
using OpenQA.Selenium;

namespace PageMapping
{
	public class SelectedCityPage : BasePage
	{
		public SelectedCityPage(IWebDriverExt<IWebDriver> driver) : base(driver)
		{
		}
		public Label OldWeather => new Label(new XpathLocator("//a[text()='Погода по-старому']"), DefaultParent);
	}
}
		