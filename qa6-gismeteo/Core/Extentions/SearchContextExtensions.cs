﻿using System;
using System.Collections.ObjectModel;
using Core.Helpers;
using Core.Interfaces;
using Core.UIControls.Locators;
using OpenQA.Selenium;
using OpenQA.Selenium.Internal;

namespace Core.Extentions
{
	public static class SearchContextExtensions
	{
		public static IWebDriver TryCastToWebDriver(this ISearchContext context)
		{
			var webDriver = context as IWebDriver;
			if (webDriver != null)
				return webDriver;

			var wrapsDriver = context as IWrapsDriver;

			if (wrapsDriver != null)
				webDriver = wrapsDriver.WrappedDriver;
			else
				IoCResolver.TestLogger.LogWarning("Failed to cast elements parent object to IWebDriver");
			return webDriver;
		}

		public static void WaitForPageLoading(this ISearchContext scope)
		{
			var webDriver = scope.TryCastToWebDriver();
			if (webDriver != null)
			{
				IoCResolver.WebDriver.WaitForPageLoading();
			}
		}

		public static string TakeScreenshot(this ISearchContext baseElement)
		{
			if (!IoCResolver.Settings.TakeErrorScreens)
				return String.Empty;

			var webDriver = baseElement.TryCastToWebDriver();
			if (webDriver != null)
				return IoCResolver.WebDriver.TakeScreenshot();

			return String.Empty;
		}

		public static T ExecuteScript<T>(this ISearchContext context, string script, params object[] args)
		{
			return ExecuteScript<T>(context, script, true, args);
		}

		public static T ExecuteScript<T>(this ISearchContext context, string script, bool needLogging = true, params object[] args)
		{
			var driver = context.TryCastToWebDriver();
			return driver == null ? default(T) : IoCResolver.WebDriver.ExecuteScript<T>(script, needLogging, args);
		}

		public static IWebElement LookupElement(this ISearchContext scope, ILocator locator)
		{
			IWebElement element = null;
			try
			{
				element = scope.FindElement(locator.By);
				if (element == null)
				{
					WaitForPageLoading(scope);
				}
			}
			catch (Exception)
			{
				// ignored
			}

			if (element == null)
				IoCResolver.TestLogger.LogWarning(string.Format("Failed to find element by {0} locator '{1}'", locator.GetType().Name, locator.Value));

			return element;
		}

		public static bool IsElementPresent(this ISearchContext context, ILocator locator)
		{
			try
			{
				context.FindElement(locator.By);
				return true;
			}
			catch (NoSuchElementException)
			{
				return false;
			}
		}

		public static IWebElement GetWebElement(this ISearchContext context, ILocator locator)
		{
			try
			{
				return context.FindElement(locator.By);
			}
			catch (NoSuchElementException ex)
			{
				var info = String.Format("Unable to find element by {0} locator '{1}'", locator.GetType().Name, locator.Value);
				IoCResolver.TestLogger.LogError(info, null, ex);
				return null;
			}
			catch (Exception ex)
			{
				var info = String.Format("Unable to find element by {0} locator '{1}'", locator.GetType().Name, locator.Value);
				IoCResolver.TestLogger.LogError(info, null, ex);
				return null;
			}
		}

		public static ReadOnlyCollection<IWebElement> GetWebElements(this ISearchContext context, ILocator locator)
		{
			try
			{
				var elements = context.FindElements(locator.By);
				return elements.Count != 0 ? elements : null;
			}
			catch (Exception ex)
			{
				var info = String.Format("Unable to find list of elements by {0} locator '{1}'", locator.GetType().Name, locator.Value);
				IoCResolver.TestLogger.LogError(info, null, ex);
				return null;
			}
		}

		public static bool WaitForCondition(this ISearchContext obj, Func<ISearchContext, bool> condition, int maxTimeout = 20, int pollingInterval = 200, string logNotes = "")
		{
			return WaitHelper.WaitForCondition(obj, condition, maxTimeout, pollingInterval, logNotes);
		}
	}
}
