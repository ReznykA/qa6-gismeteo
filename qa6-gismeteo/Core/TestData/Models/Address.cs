﻿namespace Core.TestData.Models
{
	public class Address
	{
		public string StreetName { get; set; }
		public string HouseNumber { get; set; }
		public string HouseLetter { get; set; }
		public string Zip { get; set; }
		public string City { get; set; }
		public string Country { get; set; }

		public string GetFullAddress()
		{
			return StreetName + " " + HouseNumber + HouseLetter + ", " + Zip + " " + City;
		}

		public override string ToString()
		{
			return GetFullAddress();
		}
	}
}