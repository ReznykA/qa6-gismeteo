﻿using System;
using System.Xml.Serialization;
using Core.Helpers;
using Core.TestData.Settings;

namespace Core.TestData
{
	[Serializable]
	public class TestDataSettings
	{

		[XmlElement("AddressInfoSettings")]
		public AddressSettings Addresses { get; set; }

		public TestDataSettings()
		{
			var testDataPropertiesPath = PathHelper.CombineXmlSourcePath(PathHelper.TestDataDir, "testdata");
			var testDataSettings = FileHelper.DeserializeFromXml<TestDataSettings>(testDataPropertiesPath);
			Addresses = testDataSettings.Addresses;
		}
	}
}
