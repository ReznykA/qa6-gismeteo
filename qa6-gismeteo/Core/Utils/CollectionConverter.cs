﻿
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace SeleniumTests.Utils
{
    public static class CollectionConverter
    {
        public static ICollection<String> ToInnerTextList(ICollection<IWebElement> webElements)
        {
            ICollection<String> res = new List<String>();
            foreach (IWebElement element in webElements)
                res.Add(element.Text);
            return res;
        }

        public static ICollection<int> ToIntList(ICollection<IWebElement> webElements)
        {
            ICollection<int> res = new List<int>();
            foreach (IWebElement element in webElements)
                res.Add(Convert.ToInt16(element.Text));
            return res;
        }

        public static List<String> ToList(ReadOnlyCollection<IWebElement> webElements)
        {
            var x = webElements.ToList();
            List<string> res = new List<string>();
            foreach (IWebElement element in x)
                res.Add(element.Text);
            return res;
        }
    }
}
