﻿using System;
using System.Linq;
using System.Threading;
using Core.Interfaces;

namespace Core.Helpers
{
	public static class WaitHelper
	{
		public static Func<IBaseElement, bool> ElementIsDisplayedFunc = e1 => e1.IsDisplayed;

		public static bool WaitForCondition<T>(T obj, Func<T, bool> condition, int maxTimeout = 20,
			int pollingInterval = 200, string logNotes = "")
		{
			var waitUntil = DateTime.Now.AddSeconds(Math.Abs(maxTimeout));
			Exception e = null;

			do
			{
				try
				{
					var result = condition(obj);
					if (result)
					{
						return true;
					}
				}
				catch (Exception exception)
				{
					e = exception;
					Thread.Sleep(pollingInterval);
				}
			} while (DateTime.Now < waitUntil);

			var info = string.Format("Wait for condition failed after timeout in {0} seconds for '{1}'.", maxTimeout,
				obj.GetType());
			IoCResolver.TestLogger.LogWarning(info + logNotes, null, e);

			return false;
		}

		private static bool WaitFunc<TElement>(Func<TElement[], bool> conditionFunc, TElement[] elements, int timeout)
		{
			// pollingInterval = 600ms for default timeout.
			var pollingInterval = timeout / 50;
			for (var i = 0; i < timeout;)
			{
				if (conditionFunc.Invoke(elements))
					return true;
				Thread.Sleep(pollingInterval);
				i += pollingInterval;
			}
			return false;
		}

		private static bool WaitFunc<TElement>(Func<TElement, bool> conditionFunc, TElement element, int timeout)
		{
			// pollingInterval = 600ms for default timeout.
			var pollingInterval = timeout / 50;
			for (var i = 0; i < timeout;)
			{
				if (conditionFunc.Invoke(element))
					return true;
				Thread.Sleep(pollingInterval);
				i += pollingInterval;
			}
			return false;
		}

		public static bool WaitUntilElementIsDisplayed(IBaseElement baseElement, int timeout)
		{
			var result = WaitFunc(e => e.IsDisplayed, baseElement, timeout);
			if (!result)
				IoCResolver.WebDriver.TakeScreenshot();
			return result;
		}

		public static bool WaitUntilInnerTextIsChangedTo(IBaseElement baseElement, string textToChangeTo, int timeout)
		{
			var result = WaitFunc(e => e.InnerText.Equals(textToChangeTo), baseElement, timeout);
			if (!result)
				IoCResolver.WebDriver.TakeScreenshot();
			return result;
		}

		public static bool WaitUntilInnerTextIsNotEqualTo(IBaseElement baseElement, string textToDisappear, int timeout)
		{
			var result = WaitFunc(e => !e.InnerText.Equals(textToDisappear), baseElement, timeout);
			if (!result)
				IoCResolver.WebDriver.TakeScreenshot();
			return result;
		}

		public static bool WaitForElementByCondition(IBaseElement baseElement, Func<IBaseElement, bool> conditionFunc, int timeout)
		{
			var result = WaitFunc(conditionFunc, baseElement, timeout);
			if (!result)
				IoCResolver.WebDriver.TakeScreenshot();
			return result;
		}

		public static bool WaitUntilElementIsClickable(IBaseElement baseElement, int timeout)
		{
			var result = WaitFunc(e => e.IsClickable, baseElement, timeout);
			if (!result)
				IoCResolver.WebDriver.TakeScreenshot();
			return result;
		}

		public static bool WaitUntilElementIsChecked(IBaseElement baseElement, int timeout)
		{
			var result = WaitFunc(e => e.GetAttribute("checked") != null, baseElement, timeout);
			if (!result)
				IoCResolver.WebDriver.TakeScreenshot();
			return result;
		}

		public static bool WaitUntilInnerTextIsNotEmpty(IBaseElement baseElement, int timeout = WaitTimeout.Small)
		{
			var result = WaitFunc(e => !e.InnerText.Equals(string.Empty), baseElement, timeout);
			if (!result)
				IoCResolver.WebDriver.TakeScreenshot();
			return result;
		}


		public static bool WaitUntilElementIsHidden(IBaseElement baseElement, int timeout)
		{
			var result = WaitFunc(e => !e.IsDisplayed, baseElement, timeout);
			if (!result)
				IoCResolver.WebDriver.TakeScreenshot();
			return result;
		}

		public static bool WaitUntilAtLeastOneElementIsDisplayed(IBaseElement element1, IBaseElement element2, int timeout)
		{
			var result = WaitFunc(elements => elements.First().IsDisplayed || elements.ElementAt(1).IsDisplayed, new[] { element1, element2 }, timeout);
			if (!result)
				IoCResolver.WebDriver.TakeScreenshot();
			return result;
		}

		public static bool WaitUntilEitherOfElementsIsDisplayed(int timeout, params IBaseElement[] elements)
		{
			var result = WaitFunc(els => els.Count(e => e.IsDisplayed) == 1, elements, timeout);

			if (!result)
				IoCResolver.WebDriver.TakeScreenshot();
			return result;
		}

		public static void TryAction(Action action, int attempts = 1)
		{
			Exception e = new Exception();
			int count = 0;
			for (int i = 0; i < attempts; i++)
			{
				try
				{
					action();
				}
				catch (Exception exception)
				{
					count++;
					e = exception;
					continue;
				}
				break;
			}
			if (count == attempts)
				throw new Exception("Action was broken after " + attempts + " attempts due to ", e);
		}

	}

	public class WaitTimeout
	{
		private const int MillisecondsInSecond = 1000;

		public const int Tiny = 5 * MillisecondsInSecond;
		public const int Small = 30 * MillisecondsInSecond;
		public const int Middle = 60 * MillisecondsInSecond;
		public const int Long = 100 * MillisecondsInSecond;
		public const int ExtraLong = 150 * MillisecondsInSecond;
	}
}
