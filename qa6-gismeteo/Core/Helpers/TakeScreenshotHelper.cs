﻿using System;
using OpenQA.Selenium;

namespace Core.Helpers
{
	public class TakeScreenshotHelper
	{
		public static int? TotalHeight
		{
			get
			{
				try
				{
					var totalHeightObj = IoCResolver.WebDriver.ExecuteScript<object>("return document.body.scrollHeight;");
					if (totalHeightObj == null)
						return null;
					return int.Parse(totalHeightObj.ToString());
				}
				catch (NullReferenceException e)
				{
					IoCResolver.TestLogger.LogError("Failed to take height", null, e);
					return null;
				}
			}
		}

		public static int? ViewportHeight
		{
			get
			{
				var viewPortHeightObj = IoCResolver.WebDriver.ExecuteScript<object>("return window.innerHeight");
				if (viewPortHeightObj == null)
					return null;
				return int.Parse(viewPortHeightObj.ToString());
			}
		}

		public static string TakeSingleScreenshot(ITakesScreenshot takeScreen, int zoomFactor, string screenName)
		{
			if (zoomFactor < IoCResolver.WebDriver.PageDefaultZoomFactor)
				IoCResolver.WebDriver.ZoomBy(zoomFactor);

			try
			{
				takeScreen.GetScreenshot().SaveAsFile(screenName, ScreenshotImageFormat.Jpeg);
				return PathHelper.CropPath(screenName, PathHelper.LogDir);
			}
			catch (Exception exception)
			{
				IoCResolver.TestLogger.LogError("Failed to take screenshot", null, exception);
			}
			finally
			{
				IoCResolver.WebDriver.ZoomToDefault();
			}

			return string.Empty;
		}


	}
}
