﻿using System;
using System.IO;
using NUnit.Framework;

namespace Core.Helpers
{
	public class PathHelper
	{
		public const string ToSolutionParentDir = @"\..\..\..";
		public const string ToProjectParentDir = @"\..\..\";
		public const string ScreenDir = "Screens";
		public const string LogDir = "TestRunLogs";
		public const string TestDataDir = "TestData";

		private static string _screensCurrent;

		// AppDomain.CurrentDomain.BaseDirectory is also acceptable but TestContext.CurrentContext.TestDirectory is recommended
		private static readonly string CurrentTestDirectory = TestContext.CurrentContext.TestDirectory;
		private static readonly string SolutionParentDir = CurrentTestDirectory.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar) + ToSolutionParentDir;
		public static readonly string ProjectParentDir = CurrentTestDirectory.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar) + ToProjectParentDir;

		public static string Screens => Path.Combine(LogDirectory, ScreenDir);

		public static string ScreensCurrent
		{
			get { return _screensCurrent ?? Screens; }
			set { _screensCurrent = value; }
		}

		public static string LogDirectory => Path.Combine(SolutionParentDir, LogDir);

		public static string CombineXmlSourcePath(string slnDir, string fileName)
		{
			return CombineFullPath(fileName, path: Path.Combine(SolutionParentDir, slnDir));
		}

		public static string CombineCsvSourcePath(string slnDir, string fileName)
		{
			return CombineFullPath(fileName, extension: Ext.Csv, path: Path.Combine(SolutionParentDir, slnDir));
		}

		public static string CombineFullPath(string fileName, string extension = null, string path = null)
		{
			fileName = fileName.Contains(".") ? fileName : $"{fileName}.{extension ?? Ext.Xml}";

			return Path.Combine(path ?? SolutionParentDir, fileName);
		}

		public static string CropPath(string path, string dirToStart)
		{
			return path.Substring(path.IndexOf(dirToStart, StringComparison.Ordinal));
		}
		
		internal class Ext
		{
			public static string Xml = "xml";
			public static string Jpg = "jpg";
			public static string Csv = "csv";
		}
	}
}
