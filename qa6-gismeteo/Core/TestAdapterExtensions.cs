﻿using System.Linq;
using NUnit.Framework;

namespace Core
{
	public static class TestAdapterExtensions
	{
		public static string GetSuiteName(this TestContext.TestAdapter adapter)
		{
			var nameParts = adapter.FullName.Split('.');
			return nameParts.Skip(nameParts.Count() - 1).FirstOrDefault();
		}

		public static string GetTestName(this TestContext.TestAdapter adapter)
		{
			return adapter.FullName.Split('.').LastOrDefault();
		}
	}
}
