﻿using Core.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Core
{
	public abstract class BasePage : LoadableComponent<BasePage>
	{
		protected readonly IWebDriverExt<IWebDriver> Driver;

		protected BasePage(IWebDriverExt<IWebDriver> driver)
		{
			Driver = driver;
		}

		public ISearchContext DefaultParent => Driver.WebDriver;

		protected override void ExecuteLoad()
		{
		}

		protected override bool EvaluateLoadedStatus()
		{
			return true;
		}
	}
}