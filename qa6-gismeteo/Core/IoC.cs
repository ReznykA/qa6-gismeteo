﻿using System;
using Core.Extentions;
using Core.Helpers;
using Core.Interfaces;
using Core.TestData;
using Core.TestLogging;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using StructureMap;

namespace Core
{
	public static class Ioc
	{

		private static IContainer _container;

		public static IContainer Container
		{
			get
			{
				if (_container != null)
				{
					return _container;
				}

				_container = new Container();
				_container.Configure(x => x.For<IContainer>().Singleton().Use(_container));
				_container.Configure(x => x.For(typeof(INamedInstanceFactory<>)).Use(typeof(NamedInstanceFactory<>)));
				_container.Configure(_ =>
				{
					_.For(typeof(INamedInstanceFactory<>)).Use(typeof(UiTestsNamedInstanceFactory<>));
					_.For<ISettings>().Use<AppConfiguration>();
					_.For<ILogger>().Use<TestLoggerNLog>();
					_.For<TestLogger>().Use<TestLogger>();
					_.For<IWebDriverExt<IWebDriver>>().Use(x => new WebDriverExt<IWebDriver>(WebDriverFunc.Invoke()));
					_.For<TestDataSettings>().Use<TestDataSettings>();
					
				});

				return _container;
			}
		}

		private static readonly Func<IWebDriver> WebDriverFunc = () =>
		{
			switch (IoCResolver.Settings.WebDriver)
			{
				case ("ChromeDriver"):
					return new ChromeDriver(TestContext.CurrentContext.TestDirectory);
				case ("FirefoxDriver"):
					return new FirefoxDriver();
			}
			return null;
		};
	}
}
