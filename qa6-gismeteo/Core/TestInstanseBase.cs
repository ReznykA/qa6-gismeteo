﻿using System;
using System.IO;
using System.Linq;
using Core.Helpers;
using Core.Interfaces;
using Core.TestData;
using Core.TestLogging;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Core
{
	public abstract class TestInstanceBase
	{
		protected TestDataSettings TestData { get; }

		public TestLogger TestLogger { get; }

		protected static IWebDriverExt<IWebDriver> Browser;

		protected ISettings Settings { get; }
		
		protected TestInstanceBase()
		{
			TestLogger = IoCResolver.TestLogger;
			Settings = IoCResolver.Settings;
			//TestData = Ioc.Container.GetInstance<TestDataSettings>();
		}

		protected abstract void InitPages();

		#region Test/Fixture Setup

		
		[OneTimeSetUp]
		public void DefaultFixtureSetup()
		{
			PathHelper.ScreensCurrent = Path.Combine(PathHelper.Screens,
				String.Concat(SuiteName, DateTime.Now.ToString("yyMMddHHmm")));

			FileHelper.EnsureDirectoryExists(PathHelper.ScreensCurrent);
		}

		[OneTimeTearDown]
		public void DefaultFixtureTearDown()
		{
			Browser?.ShutDown();
		}

		[TearDown]
		public void DefaultTestTearDown()
		{
			try
			{
				Browser.GetCurrentUrl();
			}
			catch (Exception e)
			{
				TestLogger.LogError("Test broken due to " + e.Message, null, e);
			}
		}

		#endregion

		protected void OpenSite(string url)
		{
			OpenBrowser();
			Browser.FollowLink(url, 1);
		}

		protected bool IsCookiePresent(string cookieName, IWebDriver driver)
		{
			return driver.Manage().Cookies.AllCookies.Any(c => c.Name.Equals(cookieName, StringComparison.OrdinalIgnoreCase));
		}

		protected void FollowLink(string link)
		{
			Browser.FollowLink(link);
		}


		#region Private Methods
		protected void OpenBrowser()
		{
			if (IoCResolver.WebDriver != null)
				return;
			Browser = Ioc.Container.GetInstance<IWebDriverExt<IWebDriver>>();
			InitPages();
			Browser.WebDriver.Manage().Timeouts().PageLoad = TimeSpan.FromMinutes(3);
			IoCResolver.WebDriver = Browser;
			Browser.Maximize();
		}

		private string SuiteName => TestContext.CurrentContext.Test.GetSuiteName();
		
		#endregion
	}

}