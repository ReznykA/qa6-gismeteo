﻿using System;
using Core.Helpers;
using Core.Interfaces;
using NUnit.Framework;

namespace Core
{
	[SetUpFixture]
	public abstract class TestRunSetupBase
	{
		[OneTimeSetUp]
		public void RunBeforeAllTests()
		{
			PrepareFileSystem();
		}

		[OneTimeTearDown]
		public void ExecuteAfterAllTests()
		{
		}

		private void PrepareFileSystem()
		{
			ISettings settings = IoCResolver.Settings;

			if (settings.CleanUpLogDir)
			{
				FileHelper.CleanUpDirectory(PathHelper.LogDirectory);
			}

			FileHelper.EnsureDirectoryExists(PathHelper.LogDirectory);

			if (settings.TakeErrorScreens)
			{
				FileHelper.EnsureDirectoryExists(PathHelper.Screens);
			}
		}
	}
}
