﻿using System.Text;
using NLog;
using NLog.LayoutRenderers;

namespace Core.TestLogging
{
	public abstract class BaseLayoutRenderer : LayoutRenderer
	{
		protected abstract string PropertyName { get; }

		protected override void Append(StringBuilder builder, LogEventInfo logEvent)
		{
			if (logEvent.Properties == null || !logEvent.Properties.ContainsKey(PropertyName))
				return;

			object prop = logEvent.Properties[PropertyName];

			if (prop != null)
				builder.Append(prop.ToString());
		}

	}
}
