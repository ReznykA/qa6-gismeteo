﻿using NLog.Config;

namespace Core.TestLogging
{
	public static class RenderersConfig
	{
		public static void Register()
		{
			ConfigurationItemFactory.Default.LayoutRenderers
				.RegisterDefinition("TestLogLevel", typeof(TestLogLevelLayoutRenderer));
			ConfigurationItemFactory.Default.LayoutRenderers
				.RegisterDefinition("SuiteName", typeof(SuiteNameLayoutRenderer));
			ConfigurationItemFactory.Default.LayoutRenderers
				.RegisterDefinition("TestName", typeof(TestNameLayoutRenderer));
			ConfigurationItemFactory.Default.LayoutRenderers
				.RegisterDefinition("TestId", typeof(StepInfoLayoutRenderer));
			ConfigurationItemFactory.Default.LayoutRenderers
				.RegisterDefinition("ScreenshotPath", typeof(ScreenshotPathLayoutRenderer));
		}
	}
}