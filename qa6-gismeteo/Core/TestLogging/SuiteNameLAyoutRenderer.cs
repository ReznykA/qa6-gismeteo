﻿using NLog.LayoutRenderers;

namespace Core.TestLogging
{
	[LayoutRenderer("SuiteName")]
	public class SuiteNameLayoutRenderer : BaseLayoutRenderer
	{
		protected override string PropertyName => "SuiteName";
	}
}
