﻿using System;
using System.Text;
using Core.Enums;
using NLog;
using ILogger = Core.Interfaces.ILogger;

namespace Core.TestLogging
{
	public class TestLoggerNLog : ILogger
	{
		private readonly Logger _logger;

		static TestLoggerNLog()
		{
			RenderersConfig.Register();
		}

		public TestLoggerNLog()
		{
			_logger = LogManager.GetCurrentClassLogger();
		}

		public void RegisterLoggingRuleByTargetName(string name)
		{
		//TODO: check usages
//			var config = LogManager.Configuration;
//			var loggingRule = new LoggingRule("*", LogLevel.Debug, ConfigureDatabaseTarget(name));
//			config.LoggingRules.Add(loggingRule);
//
//			LogManager.Configuration = config;
		}

		public void LogEvent(TestLogLevel level, string testName, string suiteName, string stepInfo)
		{
			LogInternal(level, testName, suiteName, stepInfo, null, null, null);
		}

		public void LogEvent(TestLogLevel level, string testName, string suiteName, string stepInfo, string screenshotPath)
		{
			LogInternal(level, testName, suiteName, stepInfo, screenshotPath, null, null);
		}

		public void LogEvent(TestLogLevel level, string testName, string suiteName, string stepInfo, string screenshotPath, string message)
		{
			LogInternal(level, testName, suiteName, stepInfo, screenshotPath, message, null);
		}

		public void LogEvent(TestLogLevel level, string testName, string suiteName, string stepInfo, string screenshotPath, Exception ex)
		{
			LogInternal(level, testName, suiteName, stepInfo, screenshotPath, null, null);
		}

		private void LogInternal(TestLogLevel level, string testName, string suiteName, string stepInfo, string screenshotPath,
			string message, Exception exception)
		{
			var evt = LogEventInfo.Create(GetNLogLogLevel(level), null, exception, null, message, null);

			evt.Properties["TestLogLevel"] = (int)level;
			evt.Properties["TestName"] = testName.Substring(0, Math.Min(200, testName.Length));
			evt.Properties["SuiteName"] = suiteName.Substring(0, Math.Min(200, suiteName.Length));
			evt.Properties["StepInfo"] = stepInfo.Substring(0, Math.Min(2000, stepInfo.Length));
			evt.Properties["ScreenshotPath"] = screenshotPath ?? "";

			if (string.IsNullOrWhiteSpace(message) && exception != null)
			{
				var sb = new StringBuilder();

				sb.Append(exception.Message);

				if (exception.InnerException != null)
				{
					sb.Append("\t");
					sb.Append("INNER EXCEPTION: ");
					sb.Append(exception.InnerException);
				}
				evt.Message = sb.ToString().Replace("\n", "").Replace("\r", "");
			}

			_logger.Log(evt);
		}


		private LogLevel GetNLogLogLevel(TestLogLevel src)
		{
			switch (src)
			{
				case TestLogLevel.Debug:
					return LogLevel.Debug;

				case TestLogLevel.Info:
					return LogLevel.Info;

				case TestLogLevel.Warn:
					return LogLevel.Warn;

				case TestLogLevel.Error:
					return LogLevel.Error;

				case TestLogLevel.Fatal:
					return LogLevel.Fatal;

				case TestLogLevel.Step:
					return LogLevel.Info;

				default:
					throw new InvalidOperationException("Unknown NLog log level " + src);
			}
		}
	}
}
