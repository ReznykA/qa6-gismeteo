﻿using NLog.LayoutRenderers;

namespace Core.TestLogging
{
	[LayoutRenderer("TestLogLevel")]
	public class TestLogLevelLayoutRenderer : BaseLayoutRenderer
	{
		protected override string PropertyName => "TestLogLevel";
	}
}
