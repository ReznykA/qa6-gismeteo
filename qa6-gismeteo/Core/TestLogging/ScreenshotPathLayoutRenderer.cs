﻿using NLog.LayoutRenderers;

namespace Core.TestLogging
{
	[LayoutRenderer("ScreenshotPath")]
	public class ScreenshotPathLayoutRenderer : BaseLayoutRenderer
	{
		protected override string PropertyName => "ScreenshotPath";
	}
}
