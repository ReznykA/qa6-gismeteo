﻿using System;
using Core.Enums;
using Core.Interfaces;
using NUnit.Framework;

namespace Core.TestLogging
{
	public class TestLogger
	{
		private static ILogger _logger;

		public TestLogger(ILogger logger)
		{
			_logger = logger;
		}

		public void LogStep(string stepInfo)
		{
			_logger.LogEvent(TestLogLevel.Step,
			TestContext.CurrentContext.Test.GetTestName(),
			TestContext.CurrentContext.Test.GetSuiteName(),
			stepInfo);
		}

		public void LogInfo(string stepInfo)
		{
			_logger.LogEvent(TestLogLevel.Info,
			TestContext.CurrentContext.Test.GetTestName(),
			TestContext.CurrentContext.Test.GetSuiteName(),
			stepInfo);
		}

		public void LogError(string stepInfo)
		{
			_logger.LogEvent(TestLogLevel.Error,
			TestContext.CurrentContext.Test.GetTestName(),
			TestContext.CurrentContext.Test.GetSuiteName(),
			stepInfo);
		}

		public void LogError(string stepInfo, string screenshotPath, Exception exception)
		{
			_logger.LogEvent(TestLogLevel.Error, 
				TestContext.CurrentContext.Test.GetTestName(),
				TestContext.CurrentContext.Test.GetSuiteName(),
				stepInfo, 
				screenshotPath, 
				exception);
		}

		public void LogWarning(string stepInfo)
		{
			_logger.LogEvent(TestLogLevel.Warn, 
			TestContext.CurrentContext.Test.GetTestName(),
			TestContext.CurrentContext.Test.GetSuiteName(),
			stepInfo);
		}

		public void LogWarning(string stepInfo, string screenshotPath, Exception exception)
		{
			_logger.LogEvent(TestLogLevel.Warn, 
				TestContext.CurrentContext.Test.GetTestName(),
				TestContext.CurrentContext.Test.GetSuiteName(),
				stepInfo,
				screenshotPath,
				exception);
		}		
		
		public void LogEvent(TestLogLevel level, string stepInfo, string screenshotPath, Exception exception)
		{
			_logger.LogEvent(level, 
				TestContext.CurrentContext.Test.GetTestName(),
				TestContext.CurrentContext.Test.GetSuiteName(),
				stepInfo,
				screenshotPath,
				exception);
		}

	}
}