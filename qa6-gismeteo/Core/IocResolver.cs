﻿using System;
using Core.Interfaces;
using Core.TestData;
using Core.TestLogging;
using OpenQA.Selenium;

namespace Core
{
	public static class IoCResolver
	{
		public static Lazy<ISettings> _settings = Resolve<ISettings>();
		//public static Lazy<TestDataSettings> _testDataSettings = Resolve<TestDataSettings>();

		public static Lazy<TestLogger> _logger = Resolve<TestLogger>();

		public static IWebDriverExt<IWebDriver> WebDriver;

		public static ISettings Settings => _settings.Value;

		public static TestLogger TestLogger => _logger.Value;
		
		//public static TestDataSettings TestData => _testDataSettings.Value;



		private static Lazy<T> Resolve<T>()
		{
			return new Lazy<T>(() => Ioc.Container.GetInstance<T>());
		}
	}
}
