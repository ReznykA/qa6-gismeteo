﻿using System;

namespace Core.Interfaces
{
	public interface INamedInstanceFactory<out T> where T : class
	{
		T Get(string name);
		T Get(Enum name);
		T TryGet(string name);
		T TryGet(Enum name);
	}
}