﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using Core.Enums;
using Core.Extentions;
using Core.Helpers;
using Core.Interfaces;
using Core.TestLogging;
using Core.UIControls.Locators;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Core.UIControls
{
	public abstract class BaseElement : LoadableComponent<BaseElement>, IBaseElement
	{
		public string ElementName { get; protected set; }
		protected const string NamePrefix = "get_";

		public ISearchContext Parent { get; private set; }
		public ILocator Locator { get; private set; }

		protected readonly ISettings Settings;
		protected readonly TestLogger Logger;

		protected BaseElement()
		{
			Settings = IoCResolver.Settings;
			Logger = IoCResolver.TestLogger;
		}

		public virtual IWebElement Object => Parent.LookupElement(Locator);

		public bool IsPresent => Object != null;

		public bool IsNotPresent => !IsPresent;

		public string Value => GetAttribute("value");

		public bool IsDisplayed
		{
			get { return Do(x => x.Displayed); }
		}

		public bool IsNotDisplayed => !IsDisplayed;

		public bool IsClickable => IsDisplayed && IsEnabled;

		public string InnerText
		{
			get { return Do(x => x.Text.Trim()); }
		}

		public bool IsEnabled
		{
			get { return Do(x => x.Enabled); }
		}

		public bool IsDisabled => !IsEnabled;

		public bool HasDisabledAttribute => GetAttribute("disabled") == "true";

		protected bool GetIsDisplayed(bool needLogging)
		{
			return Do(x => x.Displayed, needLogging);
		}

		protected bool GetIsNotDisplayed(bool needLogging)
		{
			return !GetIsDisplayed(needLogging);
		}
		public void SetSearchContext(ILocator locator, ISearchContext parent)
		{
			Parent = parent;
			Locator = locator;
		}

		public virtual void Click(bool throwException = true)
		{
			if (IsClickable)
			{
				Do(x => x.Click());
			}
			else if (throwException)
			{
				IoCResolver.WebDriver.TakeScreenshot();
				throw new Exception($"Can't click on {ElementName ?? "element"} with {Locator.GetType().Name} locator: {Locator.Value}");
			}
		}

		public virtual void ClickAndWaitForLoading(bool throwException = true)
		{
			Click(throwException);
			Parent.WaitForPageLoading();
		}

		public string GetAttribute(string attrName)
		{
			return Do(x => x.GetAttribute(attrName));
		}

		public void SendKeys(string key)
		{
			Do(x => x.SendKeys(key));
		}

		public BaseElement ScrollTo()
		{
			var elemPos = Parent.FindElement(Locator.By).Location.Y - 200;
			Parent.ExecuteScript<object>("window.scroll(0, " + elemPos + ");");

			return this;
		}

		public bool WaitUntilIsDisplayed(int waitInterval = 20)
		{
			//Logging inside getIsDispalyed is turned OFF(there is huge number of log records). 
			//Logging by wait result is turned ON.
			return WaitUntil(x => x.GetIsDisplayed(false), waitInterval, needLogging: true);
		}

		public bool WaitUntilIsNotDisplayed(int waitInterval = 20)
		{
			//Logging inside getNotDispalyed is turned OFF(there is huge number of log records). 
			//Logging by wait result is turned ON.
			return WaitUntil(x => x.GetIsNotDisplayed(false), waitInterval, needLogging:true);
		}

		protected bool WaitUntil(Func<BaseElement, bool> conditionOnElement, int waitInterval = 20, string logNotes = "", bool needLogging = false)
		{
			bool result =  WaitForCondition(conditionOnElement, waitInterval, logNotes: MethodBase.GetCurrentMethod().Name);

			if (needLogging)
			{
				var methodName = new StackFrame(1).GetMethod().Name;
				var stepInfo = ComposeActionStepInfo(methodName, GetType().Name, Locator);
				if (result)
					LogActionDetails(stepInfo + " Successful", TestLogLevel.Info);
				else
					LogActionDetails(stepInfo + " Unsuccessful", TestLogLevel.Error);
			}
			return result;
		}

		public bool WaitForCondition(Func<BaseElement, bool> condition, int maxTimeout = 20, int pollingInterval = 200, string logNotes = "")
		{
			return WaitHelper.WaitForCondition(this, condition, maxTimeout, pollingInterval, logNotes);
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		protected void Do(Action<IWebElement> doAction, bool needLogging = true)
		{
			var methodName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
			var stepInfo = ComposeActionStepInfo(methodName, GetType().Name, Locator);

			try
			{
				if (IsPresent)
				{
					doAction(Object);
					if (needLogging)
					{
						LogActionDetails(stepInfo, TestLogLevel.Info);
					}
				}
				else
				{
					string logMessage = String.Format("{0} {1} NOT FOUND", stepInfo, ElementName ?? "Element");
					LogActionDetails(logMessage, TestLogLevel.Error, screenPath: Parent.TakeScreenshot());
				}
			}
			catch (InvalidOperationException exception)
			{
				LogActionDetails(stepInfo, TestLogLevel.Error, exception);
			}
			catch (Exception exception)
			{
				LogActionDetails(stepInfo, TestLogLevel.Error, exception, Parent.TakeScreenshot());
			}
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		protected TResult Do<TResult>(Func<IWebElement, TResult> executeFunc, bool needLogging = true)
		{
			var methodName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
			var stepInfo = ComposeActionStepInfo(methodName, GetType().Name, Locator);

			try
			{
				if (IsPresent)
				{
					var result = executeFunc(Object);
					if(needLogging)
						LogActionDetails(stepInfo + "with result: '" + result + "'", TestLogLevel.Info);
					return result;
				}
			}
			catch (InvalidOperationException exception)
			{
				LogActionDetails(stepInfo, TestLogLevel.Error, exception);
			}
			catch (Exception exception)
			{
				LogActionDetails(stepInfo, TestLogLevel.Error, exception, Parent.TakeScreenshot());
			}
			return default(TResult);
		}

		private void LogActionDetails(string stepInfo, TestLogLevel level, Exception exception = null, string screenPath = "")
		{
			Logger.LogEvent(level, stepInfo, screenPath, exception);
		}

		private string ComposeActionStepInfo(string methodName, string elementType, ILocator locator)
		{
			string element = ElementName ?? "element " + elementType;

			return $"Execute '{methodName}' for {element} with {locator.GetType().Name} locator: {locator.Value}. ";
		}
	}
}