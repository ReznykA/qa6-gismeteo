﻿using System;
using Core.Helpers;
using Core.Interfaces;
using Core.UIControls.Locators;
using OpenQA.Selenium;

namespace Core.UIControls.SimpleControls
{
	/// <summary>
	/// Can be used for any container, such as div, span, li, etc.
	/// </summary>
	public class CommonElement : BaseElement
	{
		public CommonElement(ILocator locator, ISearchContext parent, int? waitTimeout = null, Func<IBaseElement, bool> waitCondition = null)
		{
			ElementName = GetType().Name;
			ElementWaitTimeout = waitTimeout ?? WaitTimeout.Small;
			ElementWaitCondition = waitCondition ?? (e1 => e1.IsDisplayed);
			SetSearchContext(locator, parent);
		}

		public CommonElement(ILocator locator, IBaseElement parent, int? waitTimeout = null, Func<IBaseElement, bool> waitCondition = null)
			: this(locator, parent.Object, waitTimeout, waitCondition)
		{

		}

		protected int ElementWaitTimeout;
		protected Func<IBaseElement, bool> ElementWaitCondition;

		protected override void ExecuteLoad()
		{
		}

		protected override bool EvaluateLoadedStatus()
		{
			return WaitHelper.WaitForElementByCondition(this, ElementWaitCondition, ElementWaitTimeout);
		}

		public string Placeholder => GetAttribute("placeholder");

		public string TestDataValue => GetAttribute("data-test-value");

		public string ClassAttribute => GetAttribute("class");

		public bool IsEmpty()
		{
			return string.IsNullOrEmpty(InnerText);
		}

		public bool IsBiggerThan(int height, int width)
		{
			var size = Do(x => x.Size);
			return !size.IsEmpty && size.Height > height && size.Width > width;
		}

		public bool InnerTextEqualsTo(string stringToCompare)
		{
			return InnerText.Equals(stringToCompare);
		}

		public bool ValueEqualsTo(string stringToCompare)
		{
			return Value.Equals(stringToCompare);
		}
	}
}
