﻿using System;
using System.Diagnostics;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.Helpers;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class Link : CommonElement
	{
		public Link(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) 
			: base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public Link(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) 
			: base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public Link(string href, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) 
			: base(new XpathLocator($"//a[contains(@href,'{href}')]"), parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public Link(string href, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
			: base(new XpathLocator($"//a[contains(@href,'{href}')]"), parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public Link WaitUntilIsClickable()
		{
			WaitHelper.WaitUntilElementIsClickable(this, WaitTimeout.Small);
			return this;
		}
	}
}
