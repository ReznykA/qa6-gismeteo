﻿using System.Collections.Generic;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public interface ISelectable
	{
		void Select<T>(T text);
		List<DropDownOption> Options { get; }
		bool HasSelectedOption<T>(T text);
		DropDownOption SelectedOption { get; }
		string SelectedOptionText { get; }
		void SelectBySubstr<T>(T substr);
		void SelectByValue<T>(T value);
		void SelectByIndex(int index);
	}
}
