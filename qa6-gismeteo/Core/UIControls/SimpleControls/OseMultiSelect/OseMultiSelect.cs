﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls.OseMultiSelect
{
	public class OseMultiSelect : CommonElement
	{
		private Lazy<List<OseMultiSelectOption>> _options;
		private Lazy<List<OseMultiSelectOption>> _selected;
		private Button _applyButton;

		public OseMultiSelect(ILocator locator, ISearchContext parent, int? timeout = null) : base(locator, parent, timeout)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
			InitFields();
		}

		private void InitFields()
		{
			var optionSelector = Locator.Value;
			optionSelector += string.Format("{0}li", Locator is XpathLocator ? "//" : " ");
			var by = Locator is XpathLocator ? By.XPath(optionSelector) : By.CssSelector(optionSelector);

			_options = new Lazy<List<OseMultiSelectOption>>(() => Object.FindElements(by)
				.Select(x => new OseMultiSelectOption(new XpathLocator(".//input"), new XpathLocator(".//label"), x)).ToList());

			_selected = new Lazy<List<OseMultiSelectOption>>(() => Options.Where(op => op.IsChecked).ToList());

			var buttonSelector = Locator.Value;
			buttonSelector += string.Format("{0}", Locator is XpathLocator ? "//button[@data-alias='apply']" : " button[data-alias='apply']");
			var byButton = Locator is XpathLocator ? By.XPath(buttonSelector) : By.CssSelector(buttonSelector);

			_applyButton = Object.FindElements(byButton).Select(x => new Button(new XpathLocator("."), x)).First();
		}

		public List<OseMultiSelectOption> Options => _options.Value;

		public List<OseMultiSelectOption> SelectedOptions => _selected.Value;

		public Button ApplyButton => _applyButton;

		public void SelectByValue(string value)
		{
			WaitUntilIsDisplayed();
			Click();
			SelectOption(value);
			ApplyButton.ClickAndWaitForLoading();
		}

		public void UnSelectByValue(string value)
		{
			WaitUntilIsDisplayed();
			Click();
			var option = Options.SingleOrDefault(x => x.OptionValue == value);
			if (option == null || !SelectedOptions.Contains(option))
				return;
			option.Click();
			ApplyButton.ClickAndWaitForLoading();
		}

		public void SelectAll()
		{
			WaitUntilIsDisplayed();
			Click();
			foreach (var option in Options)
			{
				SelectOption(option.OptionValue);
			}
			ApplyButton.ClickAndWaitForLoading();
		}

		public void UnSelectById(string id)
		{
			WaitUntilIsDisplayed();
			Click();
			var option = Options.SingleOrDefault(x => x.OptionId == id);
			if (option == null || !SelectedOptions.Contains(option))
				return;
			option.Click();
			ApplyButton.ClickAndWaitForLoading();
		}

		private void SelectOption(string value)
		{
			var option = Options.SingleOrDefault(x=>x.OptionValue == value);
			if (option == null || SelectedOptions.Contains(option))
				return;
			option.Click();
		}
	}
}
