﻿using System;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class OseRadioButton : OseSelectableOption<RadioButton>
	{
		public OseRadioButton(ILocator innerRadioButtonLocator, ILocator innerLabelLocator, ISearchContext parent, int? timeout = null,
			Func<IBaseElement, bool> waitCondition = null)
			: base(innerLabelLocator, parent, timeout, waitCondition)
		{
			_innerSelectableOption = new RadioButton(innerRadioButtonLocator, parent);
		}

		public OseRadioButton(ILocator innerRadioButtonLocator, ILocator innerLabelLocator, IBaseElement parent, int? timeout = null,
			Func<IBaseElement, bool> waitCondition = null)
			: base(innerLabelLocator, parent, timeout, waitCondition)
		{
			_innerSelectableOption = new RadioButton(innerRadioButtonLocator, parent);
		}
	}
}
