﻿using System;
using System.Threading;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.Helpers;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class InternalMenuLink : Link
	{
		public InternalMenuLink(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
		}

		public InternalMenuLink(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
		}

		public void Navigate()
		{
			Thread.Sleep(2500);
			WaitHelper.WaitUntilElementIsClickable(this, WaitTimeout.Middle);
			ClickAndWaitForLoading();

			if (!IsActive)
			{
				WaitHelper.WaitUntilElementIsClickable(this, WaitTimeout.Small);
				IoCResolver.WebDriver.ClickElementByOffset(this, -10, -10);

				if (!IsActive)
				{
					throw new Exception("Can't select menu item: " + this);
				}
			}
		}
	}
}
