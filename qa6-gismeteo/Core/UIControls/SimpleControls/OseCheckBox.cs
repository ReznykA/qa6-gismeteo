﻿using System;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class OseCheckBox : OseSelectableOption<CheckBox>
	{
		public OseCheckBox(ILocator innerCheckBoxLocator, ILocator innerLabelLocator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
			: base(innerLabelLocator, parent, timeout, waitCondition)
		{
			_innerSelectableOption = new CheckBox(innerCheckBoxLocator, parent);
		}

		public OseCheckBox(ILocator innerCheckBoxLocator, ILocator innerLabelLocator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
			: base(innerLabelLocator, parent, timeout, waitCondition)
		{
			_innerSelectableOption = new CheckBox(innerCheckBoxLocator, parent);
		}
	}
}
