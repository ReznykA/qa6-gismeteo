﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Telenor.OneScreen.UITests.Core.Helpers;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public abstract class OseSelectableOption<TElement> : CommonElement 
		where TElement : SelectableOption
	{
		protected Label _innerLabel;
		protected TElement _innerSelectableOption;

		protected OseSelectableOption(ILocator innerLabelLocator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
		 : base(innerLabelLocator, parent, timeout, waitCondition)
		{
			_innerLabel = new Label(innerLabelLocator, parent, timeout);
		}

		protected OseSelectableOption(ILocator innerLabelLocator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
		 : base(innerLabelLocator, parent, timeout, waitCondition)
		{
			_innerLabel = new Label(innerLabelLocator, parent, timeout);
		}

		public OseSelectableOption<TElement> Check()
		{
			if (IsUnchecked)
				_innerLabel.ClickAndWaitForLoading();

			return this;
		}

		public OseSelectableOption<TElement> Uncheck()
		{
			if (IsChecked)
				_innerLabel.ClickAndWaitForLoading();

			return this;
		}

		public bool IsChecked => _innerSelectableOption.IsChecked;

		public bool IsUnchecked => !IsChecked;

		public string OptionValue => _innerSelectableOption.Value;

		public string OptionId => _innerSelectableOption.GetAttribute("id");

		public OseSelectableOption<TElement> WaitUntilIsChecked(int waitTimeout = WaitTimeout.Middle)
		{
			var result = WaitHelper.WaitUntilElementIsChecked(_innerSelectableOption, waitTimeout);

			if(!result)
				throw new LoadableComponentException("Check box is not checked: " + _innerSelectableOption.Locator);

			return this;
		}
	}
}
