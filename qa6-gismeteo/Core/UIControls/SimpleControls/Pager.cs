﻿using System.Linq;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class Pager : CommonElement
	{
		private ILocator previousPageLocator;
		private ILocator nextPageLocator;

		private ILocator pagerGridLocator;
		private ILocator pagerGridElementLocator;

		public Pager(ILocator locator, ISearchContext parent, int? waitTimeout = null) : base(locator, parent, waitTimeout)
		{
			previousPageLocator = new CssLocator("*[data-element=pagePrevious]");
			nextPageLocator = new CssLocator("*[data-element=pageNext]");
			pagerGridLocator = new CssLocator("*[data-element=pages]");
			pagerGridElementLocator = new CssLocator("*[data-element=pageButton]");
		}

		public Pager(ILocator locator, IBaseElement parent, int? waitTimeout = null) : base(locator, parent, waitTimeout)
		{
		}

		public Button PreviousPage => new Button(previousPageLocator, this);

		public GridBase<Button> PagerButtons => new GridBase<Button>(pagerGridLocator, this,
			(l, p) => new Button(l, p), pagerGridElementLocator);

		public Button NextPage => new Button(nextPageLocator, this);

		public Button SelectedPage => PagerButtons.GridElements.FirstOrDefault(pe => pe.ClassAttribute.Contains("current"));
	}
}
