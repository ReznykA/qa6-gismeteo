﻿using System;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.Helpers;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;
using Telenor.OneScreen.UITests.Core.UIControls.Locators.OseLocators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class DatePicker : InputField
	{
		public DatePicker(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitConditon = null) : base(locator, parent, timeout, waitConditon)
		{
		}

		public DatePicker(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitConditon = null) : base(locator, parent, timeout, waitConditon)
		{
		}

		public InputField InputField => new InputField(new OseCssLocator(UITests.Locators.Locators.Datepicker.InputField), this);
		public Button ClearButton => new Button(new OseCssLocator(UITests.Locators.Locators.Datepicker.ClearBtn), this);

		public void FilterByDate(DateTime from, DateTime to)
		{
			InputField.Type($"{from.GetCurrentDateFormat()} - {to.GetCurrentDateFormat()}");
			InputField.SendKeys(Keys.Enter);
			IoCResolver.WebDriver.WaitForPageLoading();
		}
	}
}
