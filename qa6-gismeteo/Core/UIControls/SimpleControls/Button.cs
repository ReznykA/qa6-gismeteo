﻿using System;
using System.Diagnostics;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Telenor.OneScreen.UITests.Core.Helpers;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class Button : CommonElement
	{
		public Button(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
			: base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}
		
		public Button(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
			: base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
		}

		public void Submit()
		{
			Do(x => x.Submit());
		}

		public Button WaitUntilIsClickable(int waitTimeOut = WaitTimeout.Middle)
		{
			var result = WaitHelper.WaitUntilElementIsClickable(this, waitTimeOut);

			if (!result)
				throw new LoadableComponentException("Button is not clickable: " + Locator);

			return this;
		}

		public override void ClickAndWaitForLoading(bool throwException = true)
		{
			base.ClickAndWaitForLoading(throwException);
			WaitHelper.WaitForElementByCondition(this, b =>
			{
				var attr = b.GetAttribute("data-loading");
				return string.IsNullOrEmpty(attr);
			}, WaitTimeout.Small);
		}
	}
}
