﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using Telenor.OneScreen.UiTests.Locators.Helpers;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class ActionsDropDown : CommonElement
	{
		private Lazy<List<Link>> _actions;

		public ActionsDropDown(ILocator locator, ISearchContext parent, int? waitTimeout = null) : base(locator, parent, waitTimeout)
		{
			var by = By.CssSelector(LocatorHelper.BuildCustomLocator(UITests.Locators.Locators.Selfcare.B2B.BulkActionsDropdownAction));
			_actions = new Lazy<List<Link>>(() => Object.FindElements(by).Select(x=>new Link(new XpathLocator("."), x)).ToList());
		}

		public void SelectByValue(string value)
		{
			WaitUntilIsDisplayed();
			Click();
			var action = _actions.Value.Single(x => x.TestDataValue == value);
			action.ClickAndWaitForLoading();
		}
	}
}
