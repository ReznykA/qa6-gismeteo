﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.Helpers;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class DropDown : CommonElement, ISelectable
	{
		private Lazy<List<DropDownOption>> _options;
		private Lazy<DropDownOption> _selected;
		private string _optionSelector;

		/// <summary>
		/// Custom element. Implements main drop-down api  for drop-downs that have options as its children.
		/// </summary>
		/// <param name="locator">Allowed XPath or Css locators.</param>
		/// <param name="parent">DOM context to find element within.</param>
		/// <param name="timeout">Non default timeout for waiting the element</param>
		/// <param name="waitCondition">Non default condition for waiting the element</param>
		public DropDown(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
			InitFields();
		}


		/// <summary>
		/// Custom element. Implements main drop-down api for drop-downs that have options as its children.
		/// </summary>
		/// <param name="locator">Allowed XPath or Css locators.</param>
		/// <param name="parent">DOM context to find element within.</param>
		/// <param name="timeout">Non default timeout for waiting the element</param>
		/// <param name="waitCondition">Non default condition for waiting the element</param>
		public DropDown(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
			ElementName = new StackFrame(1).GetMethod().Name.Replace(NamePrefix, "");
			InitFields();
		}

		protected void InitFields()
		{
			_optionSelector = Locator.Value;
			_optionSelector += string.Format("{0}", Locator is XpathLocator ? "/option" : ">option");
			var by = Locator is XpathLocator ? By.XPath(_optionSelector) : By.CssSelector(_optionSelector);

			_options = new Lazy<List<DropDownOption>>(() => Object.FindElements(by)
				.Select(x => new DropDownOption(new XpathLocator("."), x)).ToList());

			_selected = new Lazy<DropDownOption>(() => Options.FirstOrDefault(op => op.GetAttribute("selected") != null));
		}

		public List<DropDownOption> Options => _options.Value;

		public DropDownOption SelectedOption => _selected.Value;

		public string SelectedOptionValueText => _selected.Value.GetAttribute("value");

		public string SelectedOptionText => SelectedOption.InnerText;

		public void Select<T>(T text)
		{
			if (IsOptionSelected(HasSelectedOption, text))
				return;
			Click();
			new DropDownOption(new XpathLocator(string.Format("//option[text()='{0}']", text)), this).Click();
		}

		public bool HasSelectedOption<T>(T text)
		{
			return ComparerHelper.ContentContains(GetAttribute("value"), text)
				|| ((SelectedOption != null) && SelectedOption.InnerText.Contains(text.ToString()));
		}

		public void SelectBySubstr<T>(T substr)
		{
			if (IsOptionSelected(HasSelectedOption, substr))
				return;
			Click();
			new DropDownOption(new XpathLocator(string.Format("//option[contains(.,'{0}')]", substr)), this).Click();
		}

		public void SelectByValue<T>(T value)
		{
			if (IsOptionSelected(HasSelectedOption, value))
				return;
			Click();
			new CommonElement(new CssLocator(string.Format("option[value='{0}']", value)), this).Click();
		}

		public void SelectByPartialValue<T>(T value)
		{
			if (IsOptionSelected(HasSelectedOption, value))
				return;
			Click();
			new CommonElement(new CssLocator(string.Format("option[value*='{0}']", value)), this).Click();
		}

		public void SelectByIndex(int index)
		{
			if (IsOptionSelected(i => SelectedOption.Equals(Options.ElementAt(i)), index))
				return;
			Click();
			Options.ElementAt(index).Click();
		}

		private bool IsOptionSelected<T>(Func<T, bool> hasSeletedItem, T itemAttribute)
		{
			WaitUntilIsDisplayed();
			if (hasSeletedItem(itemAttribute))
				return true;
			return false;
		}
	}
}