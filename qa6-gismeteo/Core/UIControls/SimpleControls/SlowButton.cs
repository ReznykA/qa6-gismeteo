﻿using System;
using System.Threading;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.Extensions;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class SlowButton : Button
	{
		private const int WaitTimeBeforeClickInMs = 250;

		public SlowButton(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
			: base(locator, parent, timeout, waitCondition)
		{
		}

		public SlowButton(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null)
			: base(locator, parent, timeout, waitCondition)
		{
		}

		public override void Click(bool throwException = true)
		{
			Thread.Sleep(WaitTimeBeforeClickInMs);
			base.Click(throwException);
		}

		public override void ClickAndWaitForLoading(bool throwException = true)
		{
			Thread.Sleep(WaitTimeBeforeClickInMs);
			Click(throwException);
			Parent.WaitForPageLoading();
		}
	}
}
