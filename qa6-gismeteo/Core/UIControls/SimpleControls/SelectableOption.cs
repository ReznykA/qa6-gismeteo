﻿using System;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.Helpers;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public abstract class SelectableOption : CommonElement
	{
		protected SelectableOption(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
		}

		protected SelectableOption(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition)
		{
		}

		public bool IsChecked
		{
			get
			{
				const string attrNameVal = "checked";
				var checkedAttr = GetAttribute(attrNameVal);

				var isChecked = !string.IsNullOrWhiteSpace(checkedAttr) && ComparerHelper.ContentEquals(checkedAttr, attrNameVal);
				var isSelected = Do(x => x.Selected);

				return isChecked || isSelected;
			}
		}

		public bool IsUnchecked
		{
			get { return !IsChecked; }
		}

		public virtual void Check()
		{
			if (IsChecked) return;
			Click();
		}
	}
}
