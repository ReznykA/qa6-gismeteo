﻿using System;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class ErrorPopup : CommonElement
	{
		public ErrorPopup(ILocator locator, ISearchContext parent, int? waitTimeout = null,
			Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, waitTimeout, waitCondition)
		{
		}

		public ErrorPopup(ILocator locator, IBaseElement parent, int? waitTimeout = null,
			Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, waitTimeout, waitCondition)
		{
		}

		public Button CloseButton => new Button(new CssLocator("div[class*='close-btn']"), this);
	}
}