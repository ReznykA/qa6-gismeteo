﻿using System;
using OpenQA.Selenium;
using Telenor.OneScreen.UITests.Core.UIControls.Locators;

namespace Telenor.OneScreen.UITests.Core.UIControls.SimpleControls
{
	public class RadioButton : SelectableOption
	{
		public RadioButton(ILocator locator, ISearchContext parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition) { }

		public RadioButton(ILocator locator, IBaseElement parent, int? timeout = null, Func<IBaseElement, bool> waitCondition = null) : base(locator, parent, timeout, waitCondition) { }
	}
}
