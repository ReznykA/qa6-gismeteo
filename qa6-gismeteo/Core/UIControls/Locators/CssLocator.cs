﻿using OpenQA.Selenium;

namespace Core.UIControls.Locators
{
	public class CssLocator : Locator
	{
		public CssLocator(string value)
			: base(value)
		{
			By = By.CssSelector(value);
		}
	}
}