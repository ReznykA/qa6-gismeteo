﻿using OpenQA.Selenium;

namespace Core.UIControls.Locators
{
	public class NameLocator : Locator
	{
		public NameLocator(string value) : base(value)
		{
			By = By.Name(value);
		}
	}
}