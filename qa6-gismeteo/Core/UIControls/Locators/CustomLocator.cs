﻿using OpenQA.Selenium;

namespace Telenor.OneScreen.UITests.Core.UIControls.Locators
{
	/// <summary>
	/// To be used for project specific purposes and customly added html attributes. 
	/// OneScreen: attribute name data-ui-test
	/// </summary>
	public class CustomLocator : Locator
	{
		public CustomLocator(string value) : base(value)
		{
			By = By.CssSelector(string.Format("[data-ui-test={0}]", value));
		}
	}
}
