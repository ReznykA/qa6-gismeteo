﻿using OpenQA.Selenium;

namespace Core.UIControls.Locators
{
	public interface ILocator
	{
		string Value { get; set; }
		By By { get; set; }
	}
}