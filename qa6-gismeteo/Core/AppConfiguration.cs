﻿using System;
using System.Configuration;
using Core.Interfaces;

namespace Core
{
	public class AppConfiguration : ISettings
	{
		public string WebDriver => ConfigurationManager.AppSettings["WebDriver"];

		public bool TakeErrorScreens => Convert.ToBoolean(ConfigurationManager.AppSettings["TakeErrorScreens"]);

		public bool CleanUpLogDir => Convert.ToBoolean(ConfigurationManager.AppSettings["CleanUpLogDir"]);

		public string LogSeparator => ConfigurationManager.AppSettings["LogSeparator"];

		public int WaitDomLoadingTimeout => Convert.ToInt32(ConfigurationManager.AppSettings["WaitDomLoadingTimeout"]);

		public int UrlLoadingTimeout => Convert.ToInt32(ConfigurationManager.AppSettings["UrlLoadingTimeout"]);


	}
}