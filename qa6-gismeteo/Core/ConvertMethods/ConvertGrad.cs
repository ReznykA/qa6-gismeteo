﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.ConvertMethods
{
    public static class ConvertGrad
    {

        public static int CelsiusToFahrenheit(int x)
        {
            double y = (x * 9.0 / 5.0 + 32.0);
            return Convert.ToInt32(Math.Round(y));
        }
    }
}
